
export const environment = {
  production: true,
  isServer: false,
  dev: true,
  publicUrl:'http://localhost:3004',
  apiUrl:'http://localhost:3004'
};
