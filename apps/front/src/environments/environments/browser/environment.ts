
export const environment = {
  production: false,
  isServer: false,
  dev: true,
  pubUrl:'http://localhost:3004',
  apiUrl:'http://localhost:3004',
};
