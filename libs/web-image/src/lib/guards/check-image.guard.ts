import { Injectable, OnDestroy } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { ImagesFacade } from '@wt/core/+state/images';
import { untilComponentDestroyed } from '@wt/core/utils';
import { filter, map, switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CheckImageGuard implements CanActivate, OnDestroy {
  constructor(private imagesFacade: ImagesFacade,
              public router: Router) {
  }

  ngOnDestroy(): void {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const patern = /^\/(?:[a-z-0-9_]*)\/((?:[a-z-_0-9]*)+)(?:\.html)$/gi;
    const params = patern.exec(state.url);
    if (params && params.length === 2) {
      const slug = params[1];
      return this.checkImage(slug);
    }

    return of(false);
  }

  checkImage(slug: string) {
    this.imagesFacade.loadImage(slug, this.imagesFacade.fields2);
    return this.imagesFacade.loaded$
      .pipe(
        filter(item => !!item),
        switchMap(() => {
          return this.imagesFacade.getImage(slug);
        }),
        map(item => {
          if (!!item) {
            return true;
          }
          return this.router.parseUrl(`/not-found`);
        }),
        untilComponentDestroyed(this)
      );
  }

}
