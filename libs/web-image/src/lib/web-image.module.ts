import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LightboxModule } from 'ngx-lightbox';

import { WebImageComponent } from './web-image/web-image.component';
import { PopularPostsModule } from '@wt/core/components/popular-posts/popular-posts.module';
import { PostCategoriesModule } from '@wt/core/components/post-categories/post-categories.module';
import { TagCloudModule } from '@wt/core/components/tag-cloud/tag-cloud.module';
import { DateFormatModule } from '@wt/core/pipes/date-format/date-format.module';
import { CountFormatModule } from '@wt/core/pipes/count-format/count-format.module';
import { ImageModule } from '@wt/core/components/image/image.module';
import { BreadCrumbsModule } from '@wt/core/components/bread-crumbs/bread-crumbs.module';
import { SpinnerModule } from '@wt/core/components/spinner/spinner.module';
import { components } from './components';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { OtherImagesModule } from '@wt/core/components/other-images/other-images.module';
import { HeaderModule } from '@wt/core/components/header/header.module';
import { WebImageRoutingModule } from './web-image-routing.module';
import { ImagesHelperService } from '@wt/core/services/images/images-helper.service';

@NgModule({
  imports: [
    CommonModule,
    WebImageRoutingModule,
    PopularPostsModule,
    PostCategoriesModule,
    TagCloudModule,
    CountFormatModule,
    DateFormatModule,
    ImageModule,
    BreadCrumbsModule,
    SpinnerModule,
    LightboxModule,
    PerfectScrollbarModule,
    OtherImagesModule,
    HeaderModule
  ],
  declarations: [WebImageComponent, ...components],
  providers: [ImagesHelperService]
})
export class WebImageModule {

}

