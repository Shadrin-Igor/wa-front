import { ChangeDetectionStrategy, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { filter } from 'rxjs/operators';

import { ImagesFacade } from '@wt/core/+state/images';
import { ImageFullModel } from '@wt/models/image';
import { untilComponentDestroyed } from '@wt/core/utils';
import { ImagesHelperService } from '@wt/core/services/images/images-helper.service';
import { VersionsModel } from '@wt/models/file';

interface ImageWithMainFileModel extends ImageFullModel {
  mainFile?: VersionsModel
}

@Component({
  selector: 'wt-next-images',
  templateUrl: './next-images.component.html',
  styleUrls: ['./next-images.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NextImagesComponent implements OnInit, OnDestroy {
  @Input() image: ImageFullModel;
  prevImage: ImageWithMainFileModel;
  nextImage: ImageWithMainFileModel;
  loaded = false;

  constructor(private imagesFacade: ImagesFacade,
              private imagesHelper: ImagesHelperService) {
  }

  ngOnInit() {
    if (this.image) {
      if (!this.image.prevImageId && !this.image.nextImageId) {
        this.imagesFacade.loadImageNext(this.image.id, this.imagesFacade.fields2);
        this.imagesFacade.getFullImage(this.image.slug)
          .pipe(
            filter(items => !!items),
            untilComponentDestroyed(this)
          )
          .subscribe(data => {
            if (data.prevImage) {
              this.prevImage = {
                ...data.prevImage,
                mainFile: this.imagesHelper.getMainFileVersions(data.prevImage.files)
              };
            }
            if (data.nextImage) {
              this.nextImage = {
                ...data.nextImage,
                mainFile: this.imagesHelper.getMainFileVersions(data.nextImage.files)
              };
            }
            this.loaded = true;
          });
      } else {
        this.loaded = true;
        if (this.image.prevImage) {
          this.prevImage = this.image.prevImage;
        }
        if (this.image.nextImage) {
          this.nextImage = this.image.nextImage;
        }
      }
    }
  }

  ngOnDestroy(): void {
  }

}
