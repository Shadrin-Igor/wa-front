import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { AnalyticsDashboardDb } from '@wt/core/fuse-fake-db/dashboard-analytics';

@Injectable()
export class HomeService implements Resolve<any> {
  widgets: any[];

  /**
   * Constructor
   *
   * @param {HttpClient} _httpClient
   */
  constructor(
    private _httpClient: HttpClient
  ) {
  }

  /**
   * Resolver
   *
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return new Promise((resolve, reject) => {

      Promise.all([
        this.getWidgets()
      ]).then(
        () => {
          resolve();
        },
        reject
      );
    });
  }

  /**
   * Get widgets
   *
   * @returns {Promise<any>}
   */
  getWidgets(): Promise<any> {
    return new Promise((resolve, reject) => {
      const list = [];
      Object.keys(AnalyticsDashboardDb.widgets)
        .forEach((key) => {
          list[key] = AnalyticsDashboardDb.widgets[key];
        });
      this.widgets = list;
      resolve(this.widgets);
      /*this._httpClient.get('api/analytics-dashboard-widgets')
          .subscribe((response: any) => {
              this.widgets = response;
              resolve(response);
          }, reject);*/
    });
  }

  getAllWidgets(): any[] {
    const list = [];
    Object.keys(AnalyticsDashboardDb.widgets)
      .forEach((key) => {
        list[key] = AnalyticsDashboardDb.widgets[key];
      });
    return list;
  }
}
