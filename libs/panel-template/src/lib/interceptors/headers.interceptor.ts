import { Injectable } from '@angular/core';
import {
  HttpHandler,
  HttpHeaderResponse,
  HttpInterceptor,
  HttpProgressEvent,
  HttpRequest,
  HttpResponse,
  HttpSentEvent,
  HttpUserEvent
} from '@angular/common/http';
import { EMPTY, Observable, throwError } from 'rxjs';
import { switchMap } from 'rxjs/internal/operators/switchMap';
import { filter } from 'rxjs/internal/operators/filter';
import { catchError } from 'rxjs/operators';

import { AuthenticationService } from '@wt/core/services/auth/authentication.service';
import { UsersFacade, UsersState } from '@wt/core/+state/users';
import { MainConst } from '@wt/core/consts/main.const';
import { RouterFacade } from '@wt/core/+state/router/router.facade';

@Injectable()
export class HeadersInterceptor implements HttpInterceptor {
  constructor(private authenticationService: AuthenticationService,
              private routerFacade: RouterFacade,
              private usersFacade: UsersFacade) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any> | any> {
    return next.handle(request).pipe(catchError((err) => {
      const userInfo = this.authenticationService.loadAuthDataFromLocalStore();


      if (err.status === 401) {
        console.log('403', err);
      }

      if (err.status === 401) {
        if (
          (
            err.error.data &&
            (err.error.data.responseStatus || err.error.data.error) &&
            (err.error.data.responseStatus === MainConst.responseStatus.tokenNotFound ||
            err.error.data.error === MainConst.responseStatus.tokenNotFound)
          ) ||
          (
            err.error &&
            (err.error.responseStatus || err.error.error) &&
            (err.error.responseStatus === MainConst.responseStatus.tokenNotFound ||
            err.error.error === MainConst.responseStatus.tokenNotFound)
          )
          || !userInfo.token
          || !userInfo.refreshToken
        ) {
          this.authenticationService.clearData();
          this.routerFacade.goTo(['/auth'], {
            queryParams: {
              redirectTo: document.location.pathname
            }
          });

          console.log('EMPTY');
          return EMPTY;// this.goToAuth(next);
        } else {
          this.usersFacade.refreshToken(userInfo.refreshToken);
          console.log('refreshToken.reSendQuery');
          return this.reSendQuery(request, next);
        }
      }

      const error = (err.error && err.error.message) || err.statusText;
      return throwError(error);
    }));
  }

  private reSendQuery(request: HttpRequest<any>, next: HttpHandler) {
    return this.usersFacade.getUser$
      .pipe(
        filter(item => item && item.loaded),
        switchMap((data: UsersState) => {
          const newRequest = request.clone({headers: request.headers.set('Authorization', `JWT ${data.token}`)});
          return next.handle(newRequest);
        })
      );
  }

  /*  private goToAuth(next: HttpHandler) {
      const url = '/auth';
      const newRequest = new HttpRequest('GET', url, {reportProgress: true});
      return next.handle(newRequest);
    }*/
}
