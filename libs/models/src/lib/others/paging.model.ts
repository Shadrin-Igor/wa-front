export class PagingModel {
  totalCount: number;
  page?: number;
  perPage?: number;
}
