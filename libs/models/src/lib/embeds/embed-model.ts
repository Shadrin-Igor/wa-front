export interface EmbedModel {
  name: string;
  page?: number;
  perPage?: number;
  fields?: string[];
  count?: number;
}
