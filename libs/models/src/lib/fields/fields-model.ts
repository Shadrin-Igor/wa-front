export interface FieldsModel {
  fields: string[];
  [key: string]: any;
}
