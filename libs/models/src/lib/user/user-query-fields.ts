// import { ImageAttachmentQueryFields } from '../image';
import { QueryFieldsNode } from '../query-fields-adapter';

export interface UserQueryFields extends QueryFieldsNode {
  avatar?: any // ImageAttachmentQueryFields
}
