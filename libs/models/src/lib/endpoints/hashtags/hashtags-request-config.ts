import { RequestConfig } from '../../http/request/request-config';

export interface HashtagsRequestConfig extends RequestConfig { }
