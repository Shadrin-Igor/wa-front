import { HashtagsResponse } from '../../hashtags-response';

export interface HashtagsLoadResponse extends HashtagsResponse { }
