import { RequestConfig } from '../../http/request/request-config';

export interface AuthRequestConfig extends RequestConfig { }
