// import { CommentData } from '../../../../data';
import { ResponseStatus } from '../../../../http';
import { LoginSendCreateResponse } from './login-send-create-response';

export interface LoginSendCreate201 extends LoginSendCreateResponse {
    status: ResponseStatus.STATUS_201;

    body: {
        user: any; // CommentData;
    };
}
