import { ResponseStatus } from '../../../../http';
import { AuthResponse } from '../../auth-response';
import { RefreshTokenResponse } from './refresh-token-response';
import { RefreshToken200 } from './refresh-token-200';

export class RefreshTokenResponseFactory {
    static createResponse(httpResponse: any): AuthResponse {
        let response: AuthResponse;
        switch (httpResponse.status) {
            case ResponseStatus.STATUS_201:
                const response201: RefreshToken200 = {
                    status: ResponseStatus.STATUS_201,
                    body: { ...httpResponse.body }
                };
                response = response201;
                break;
            case ResponseStatus.STATUS_422:
            case ResponseStatus.STATUS_400:
            case ResponseStatus.STATUS_404:
                response = {
                    status: httpResponse.status,
                    body: httpResponse.error
                } as RefreshTokenResponse;
                break;
            default:
                response = null;
                break;

        }
        return response;
    }
}
