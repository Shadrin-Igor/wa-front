export enum AuthEndpointTypes {
    LOGIN_SEND = 'login-send',
    REFRESH_TOKEN = 'refresh-token',
}
