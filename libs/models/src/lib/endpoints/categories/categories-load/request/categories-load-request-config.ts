import {UserAuthHeaders} from '../../../../user';
import {CategoriesRequestConfig} from '../../../categories/categories-request-config';

export interface CategoriesLoadRequestConfig extends CategoriesRequestConfig {
  where: any;
  fields: any;
  perPage?: number;
  headers: UserAuthHeaders;
}
