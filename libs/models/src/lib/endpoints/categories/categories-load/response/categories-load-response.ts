import { CategoriesResponse } from '../../categories-response';

export interface CategoriesLoadResponse extends CategoriesResponse { }
