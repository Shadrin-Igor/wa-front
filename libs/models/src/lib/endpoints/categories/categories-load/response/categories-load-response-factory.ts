import { ResponseStatus } from '../../../../http';
import { CategoriesResponse } from '../../categories-response';
import { CategoriesLoadResponse } from './categories-load-response';
import { CategoriesLoad200 } from './categories-load-200';

export class CategoriesLoadResponseFactory {
    static createResponse(httpResponse: any): CategoriesResponse {
        let response: CategoriesResponse;
        switch (httpResponse.status) {
            case ResponseStatus.STATUS_200:
                const response201: CategoriesLoad200 = {
                    status: ResponseStatus.STATUS_200,
                    body: { ...httpResponse.body }
                };
                response = response201;
                break;
            case ResponseStatus.STATUS_422:
            case ResponseStatus.STATUS_400:
            case ResponseStatus.STATUS_404:
                response = {
                    status: httpResponse.status,
                    body: httpResponse.error
                } as CategoriesLoadResponse;
                break;
            default:
                response = null;
                break;

        }
        return response;
    }
}
