import { ResponseStatus } from '../../../../http';
import { ImagesResponse } from '../../images-response';
import { ImagesUpdateResponse } from './images-update-response';
import { ImagesUpdate200 } from './images-update-200';

export class ImagesUpdateResponseFactory {
    static createResponse(httpResponse: any): ImagesResponse {
        let response: ImagesResponse;
        switch (httpResponse.status) {
            case ResponseStatus.STATUS_200:
                const response201: ImagesUpdate200 = {
                    status: ResponseStatus.STATUS_200,
                    body: { ...httpResponse.body }
                };
                response = response201;
                break;
            case ResponseStatus.STATUS_422:
            case ResponseStatus.STATUS_400:
            case ResponseStatus.STATUS_404:
                response = {
                    status: httpResponse.status,
                    body: httpResponse.error
                } as ImagesUpdateResponse;
                break;
            default:
                response = null;
                break;

        }
        return response;
    }
}
