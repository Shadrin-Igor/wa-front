import { UserAuthHeaders } from '../../../../user';
import { ImagesRequestConfig } from '../../../images/images-request-config';
import { EmbedModel } from '@wt/models/embeds';

export interface ImagesGetNextRequestConfig extends ImagesRequestConfig {
  id: number | string;
  fields?: any;
}
