import { ImagesResponse } from '../../images-response';

export interface ImagesGetNextResponse extends ImagesResponse { }
