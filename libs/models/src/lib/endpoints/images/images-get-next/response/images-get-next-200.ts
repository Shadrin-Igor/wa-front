import {ResponseStatus} from '../../../../http';
import {ImagesGetNextResponse} from './images-get-next-response';

export interface ImagesGetNext200 extends ImagesGetNextResponse {
  status: ResponseStatus.STATUS_200;
  body: {
    data: {
      prevImage: any,
      nextImage: any
    }
  };
}
