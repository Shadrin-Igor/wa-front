import { ResponseStatus } from '../../../../http';
import { ImagesResponse } from '../../images-response';
import { ImagesLoadResponse } from './images-load-response';
import { ImagesLoad200 } from './images-load-200';

export class ImagesLoadResponseFactory {
    static createResponse(httpResponse: any): ImagesResponse {
        let response: ImagesResponse;
        switch (httpResponse.status) {
            case ResponseStatus.STATUS_200:
                const response201: ImagesLoad200 = {
                    status: ResponseStatus.STATUS_200,
                    body: { ...httpResponse.body }
                };
                response = response201;
                break;
            case ResponseStatus.STATUS_422:
            case ResponseStatus.STATUS_400:
            case ResponseStatus.STATUS_404:
                response = {
                    status: httpResponse.status,
                    body: httpResponse.error
                } as ImagesLoadResponse;
                break;
            default:
                response = null;
                break;

        }
        return response;
    }
}
