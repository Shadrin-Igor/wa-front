import { ResponseStatus } from '../../../../http';
import { ImagesLoadResponse } from './images-load-response';
import { MetaModel } from '@wt/models/others';

export interface ImagesLoad200 extends ImagesLoadResponse {
  status: ResponseStatus.STATUS_200;

  body: {
    data: any;
    meta?: MetaModel
  };

}
