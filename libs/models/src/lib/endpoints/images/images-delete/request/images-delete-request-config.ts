import {UserAuthHeaders} from '../../../../user';
import {ImagesRequestConfig} from '../../../images/images-request-config';
import {PagingType} from '@wt/models/entity';

export interface ImagesDeleteRequestConfig extends ImagesRequestConfig {
  id: number;
  headers: UserAuthHeaders;
  fields?: any;
  paging?: PagingType;
}
