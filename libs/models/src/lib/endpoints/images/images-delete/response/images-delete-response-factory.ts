import { ResponseStatus } from '../../../../http';
import { ImagesResponse } from '../../images-response';
import { ImagesDeleteResponse } from './images-delete-response';
import { ImagesDelete200 } from './images-delete-200';

export class ImagesDeleteResponseFactory {
    static createResponse(httpResponse: any): ImagesResponse {
        let response: ImagesResponse;
        switch (httpResponse.status) {
            case ResponseStatus.STATUS_200:
                const response200: ImagesDelete200 = {
                    status: ResponseStatus.STATUS_200,
                    body: { ...httpResponse.body }
                };
                response = response200;
                break;
            case ResponseStatus.STATUS_422:
            case ResponseStatus.STATUS_400:
            case ResponseStatus.STATUS_404:
                response = {
                    status: httpResponse.status,
                    body: httpResponse.error
                } as ImagesDeleteResponse;
                break;
            default:
                response = null;
                break;

        }
        return response;
    }
}
