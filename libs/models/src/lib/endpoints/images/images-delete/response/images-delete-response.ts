import { ImagesResponse } from '../../images-response';

export interface ImagesDeleteResponse extends ImagesResponse { }
