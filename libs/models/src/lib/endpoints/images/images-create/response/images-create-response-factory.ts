import { ResponseStatus } from '../../../../http';
import { ImagesResponse } from '../../images-response';
import { ImagesCreateResponse } from './images-create-response';
import { ImagesCreate200 } from './images-create-200';

export class ImagesCreateResponseFactory {
    static createResponse(httpResponse: any): ImagesResponse {
        let response: ImagesResponse;
        switch (httpResponse.status) {
            case ResponseStatus.STATUS_201:
                const response201: ImagesCreate200 = {
                    status: ResponseStatus.STATUS_201,
                    body: { ...httpResponse.body }
                };
                response = response201;
                break;
            case ResponseStatus.STATUS_422:
            case ResponseStatus.STATUS_400:
            case ResponseStatus.STATUS_404:
                response = {
                    status: httpResponse.status,
                    body: httpResponse.error
                } as ImagesCreateResponse;
                break;
            default:
                response = null;
                break;

        }
        return response;
    }
}
