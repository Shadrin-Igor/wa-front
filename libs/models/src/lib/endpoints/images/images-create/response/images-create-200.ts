// import { CommentData } from '../../../../data';
import { ResponseStatus } from '../../../../http';
import { ImagesCreateResponse } from './images-create-response';
import { MetaModel } from '@wt/models/others';

export interface ImagesCreate200 extends ImagesCreateResponse {
  status: ResponseStatus.STATUS_201;

  body: {
    data: any;
    meta?: MetaModel
  };
}
