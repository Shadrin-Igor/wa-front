import { UserAuthHeaders } from '../../../../user';
import { ImagesRequestConfig } from '../../../images/images-request-config';
import { EmbedModel } from '@wt/models/embeds';

export interface ImagesGetRequestConfig extends ImagesRequestConfig {
  id: number | string;
  where?: any;
  fields?: any;
  forPanel?: boolean;
  headers: UserAuthHeaders;
}
