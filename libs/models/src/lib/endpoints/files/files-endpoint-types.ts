export enum FilesEndpointTypes {
  UPDATE = 'update',
  DELETE = 'delete',
  SORT = 'sort',
  LOAD = 'load'
}
