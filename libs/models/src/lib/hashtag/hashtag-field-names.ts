export enum HashtagFieldNames {
  NAME = 'name',
  SLUG = 'slug',
  DESCRIPTION = 'description',
  IMAGE = 'image',
  COUNT = 'count',
}
