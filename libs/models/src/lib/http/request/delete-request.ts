import { Request } from './request';
import { RequestMethod } from './request-method';

export abstract class DeleteRequest extends Request {
  httpMethod = RequestMethod.DELETE;
}
