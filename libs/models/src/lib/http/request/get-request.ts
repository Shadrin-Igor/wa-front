import { Request } from './request';
import { RequestMethod } from './request-method';
import { EmbedModel } from '@wt/models/embeds';

export abstract class GetRequest extends Request {
  httpMethod = RequestMethod.GET;

  convertWHereToString(obj = {}) {
    let whereString = '';
    Object.keys(obj).forEach((key) => {
      if (whereString) {
        whereString += ',';
      }
      whereString += `${key}=${obj[key]}`;
    });
    if (whereString) {
      whereString = `where=(${whereString})`;
    }
    return whereString;
  }

  convertEmbedsToString(obj: EmbedModel[] = []) {
    const embeds = [];
    obj.forEach( item =>  {
      embeds.push(this.embedToString(item));
    });

    return embeds.length ? `&embed=${embeds.join(',')}` : '';
  }

  private embedToString(obj: EmbedModel) {
    const embedParams = [];
    if (obj.fields && obj.fields.length) {
      embedParams.push(`fields=${obj.fields.join(',')}`);
    }
    if (obj.page) {
      embedParams.push(`page=${obj.page}`);
    }
    if (obj.perPage) {
      embedParams.push(`perPage=${obj.fields.join(',')}`);
    }
    if (obj.count) {
      embedParams.push(`count=${obj.count}`);
    }
    return `${obj.name}(${embedParams.join(',')})`;
  }
}
