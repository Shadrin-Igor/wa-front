export interface RequestResultInfo {
  isValid: boolean;
  message: string;
}
