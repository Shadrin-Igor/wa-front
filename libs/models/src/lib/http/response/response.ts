import { MetaModel, PagingModel } from '@wt/models/others';

export interface Response {
  status: number;
  headers?: any;
  body: {
    meta?: MetaModel;
    data: any;
    error: any;
  } | any;

}
