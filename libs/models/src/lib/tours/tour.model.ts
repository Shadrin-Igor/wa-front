import { Entity } from '@wt/core/models/entity';

export class TourModel extends Entity{
  title: string;
  description: string;
}
