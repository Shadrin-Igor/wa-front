export interface FileModelData {
  id: string;
  title: string;
  main: boolean;
  sort: number;
  file: string;
  folder: string;
  item_id: number;
}
