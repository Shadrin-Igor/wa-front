import {FileModel} from '@wt/models/file';

export class ImageModel {
  id?: number;
  title: string;
  description: string;
  descriptionFull: string;
  slug?: string;
  isPublic?: boolean;
  files?: FileModel[];
  tagIds?: number[];
  categoryId: number;
  createdAt?: string;
  views?: number;
  prevImageId?: number;
  nextImageId?: number;
  metaDescription?: string;
  metaTitle?: string;
  metaKeyWord?: string;
}
