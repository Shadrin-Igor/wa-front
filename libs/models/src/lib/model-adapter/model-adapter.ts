import { Constructable, MapperUtil } from '@wt/models/mapper';
import { UserModel } from '@wt/models/user';
import { imageScheme, userScheme } from '@wt/models/schemes';
import { ImageModel } from '@wt/models/image';
import { FileModel } from '@wt/models/file';
import { fileScheme } from '@wt/models/schemes/file-scheme';
import {categoryScheme} from '@wt/models/schemes/category-scheme';
import {hashtagScheme} from '@wt/models/schemes/hashtag-scheme';
import {CategoryModel} from '@wt/models/category';
import {HashtagModel} from '@wt/models/hashtag';

const includeFn = (value: any, source: any) => {
  return value !== undefined;
};
export class ModelAdapter {

  private static _modelAdapter: ModelAdapter;

  private constructor() {
    MapperUtil.register(UserModel, userScheme, { include: includeFn });
    MapperUtil.register(ImageModel, imageScheme, { include: includeFn });
    MapperUtil.register(FileModel, fileScheme, { include: includeFn });
    MapperUtil.register(CategoryModel, categoryScheme, { include: includeFn });
    MapperUtil.register(HashtagModel, hashtagScheme, { include: includeFn });
  }

  public static createInstance(): ModelAdapter {
    if (!this._modelAdapter) {
      this._modelAdapter = new ModelAdapter();
    }
    return this._modelAdapter;
  }

  convertItem<T, Source>(type: Constructable<T>, item: Source): T {
    return MapperUtil.map(type, item);
  }

  convertItems<T, Source>(type: Constructable<T>, items: Source[]): T[] {
    if ( items && Array.isArray(items)) {
      return items.map(item => this.convertItem(type, item))
    } else {
      return [];
    }
  }

}
