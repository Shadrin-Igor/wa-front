import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

import { MainFacade } from '@wt/core/+state/main';
import { untilComponentDestroyed } from '@wt/core/utils';
import { MetaDataModel } from '@wt/core/models';

@Component({
  selector: 'wt-main',
  templateUrl: './main.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainComponent implements OnInit, OnDestroy {

  constructor(private mainFacade: MainFacade,
              private meta: Meta,
              private titleService: Title
  ) {
  }

  ngOnInit() {
    this.checkMedata();
  }

  ngOnDestroy() {
  }

  checkMedata() {
    this.mainFacade.getMetadata()
      .pipe(
        untilComponentDestroyed(this)
      )
      .subscribe((data: MetaDataModel) => {
        if (data) {
          if (data.title) {
            this.titleService.setTitle(data.title);
          }
          if (data.description) {
            this.meta.addTag({ name: 'description', content: data.description });
          }
          if (data.keyWords) {
            this.meta.addTag({ name: 'keywords', content: data.keyWords });
          }
        }
      });
  }
}
