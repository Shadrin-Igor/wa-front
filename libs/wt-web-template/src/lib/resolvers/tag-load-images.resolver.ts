import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, first, switchMap } from 'rxjs/operators';
import { ImagesFacade } from '@wt/core/+state/images';
import { ImageFieldNames } from '@wt/models/image';
import { CategoryFieldNames } from '@wt/models/category';
import { HashtagFieldNames, HashtagModel } from '@wt/models/hashtag';
import { HashtagsFacade } from '@wt/core/+state/hashtags';

@Injectable()
export class TagLoadImagesResolver implements Resolve<Observable<boolean>> {
  constructor(private imagesFacade: ImagesFacade,
              private hashtagsFacade: HashtagsFacade
  ) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    const url = state.url;
    const patern = /^(?:.*)(?:\/)((.*)+)(?:\.html)$/gi;
    const params = patern.exec(url);
    const slug = params[1];
    return this.getFromStoreOrAPI(slug);
  }

  getFromStoreOrAPI(slug): Observable<boolean> {
    const where = {};
    const fields = {
      fields: [
        ImageFieldNames.TITLE,
        ImageFieldNames.DESCRIPTION,
        ImageFieldNames.SLUG,
        ImageFieldNames.CATEGORY_ID,
        ImageFieldNames.CREATEDAT,
        ImageFieldNames.VIEWS
      ],
      category: {
        fields: [CategoryFieldNames.NAME]
      },
      tags: {
        fields: [HashtagFieldNames.NAME],
        count: {}
      },
      files: {
        fields: ['file', 'folder'],
        versions: {}
      },
      paging: {
        page: 1,
        perPage: 10
      }
    };
    // this.imagesFacade.clearImages();

    return this.hashtagsFacade.getHashtags(slug)
      .pipe(
        filter(item => !!item),
        switchMap((tag: HashtagModel) => {
          this.imagesFacade.loadImages({ where, fields, tagId: tag.id });
          // window.scrollTo(0, 0);
          return this.imagesFacade.waitLoading();
        }),
        first()
      );
  }
}
