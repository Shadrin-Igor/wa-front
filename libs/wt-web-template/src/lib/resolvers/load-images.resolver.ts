import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {first} from 'rxjs/operators';
import {ImagesFacade} from '@wt/core/+state/images';
import {ImageFieldNames} from '@wt/models/image';
import {CategoryFieldNames} from '@wt/models/category';
import {HashtagFieldNames} from '@wt/models/hashtag';

@Injectable()
export class LoadImagesResolver implements Resolve<Observable<boolean>> {
  constructor(private imagesFacade: ImagesFacade
  ) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.getFromStoreOrAPI();
  }

  getFromStoreOrAPI(): Observable<boolean> {
    const where = {
    };
    const fields = {
      fields: [
        ImageFieldNames.TITLE,
        ImageFieldNames.DESCRIPTION,
        ImageFieldNames.SLUG,
        ImageFieldNames.CATEGORY_ID,
        ImageFieldNames.CREATEDAT,
        ImageFieldNames.VIEWS
      ],
      category: {
        fields: [CategoryFieldNames.NAME, CategoryFieldNames.SLUG]
      },
      tags: {
        fields: [HashtagFieldNames.NAME, HashtagFieldNames.SLUG],
        count: {}
      },
      files: {
        fields: ['file', 'folder'],
        versions: {}
      },
      paging: {
        page: 1,
        perPage: 10
      }
    };
    this.imagesFacade.loadImages({where, fields});

    return this.imagesFacade.waitLoading()
      .pipe(
        first()
      );
  }
}
