import {Injectable, OnDestroy} from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree
} from '@angular/router';
import {Observable} from 'rxjs';
import {filter, first, map, switchMap} from 'rxjs/operators';
import {HashtagFieldNames, HashtagModel} from '@wt/models/hashtag';
import {HashtagsFacade} from '@wt/core/+state/hashtags';

@Injectable()
export class LoadTagGuard implements CanActivate, OnDestroy {
  constructor(private hashtagsFacade: HashtagsFacade,
              private router: Router
  ) {
  }

  ngOnDestroy(): void {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    const url = state.url;
    const patern = /^(?:.*)(?:\/)((.*)+)(?:\.html)$/gi;
    const params = patern.exec(url);
    const slug = params[1];

    const where = {
      slug
    };
    const fields = {
      fields: [
        HashtagFieldNames.NAME,
        HashtagFieldNames.SLUG,
        HashtagFieldNames.DESCRIPTION,
        HashtagFieldNames.IMAGE
      ]
    };
    this.hashtagsFacade.loadHashtags(where, fields, 1);

    return this.hashtagsFacade.waitLoading()
      .pipe(
        filter(item => !!item),
        switchMap(() => {
          return this.hashtagsFacade.getHashtags(slug);
        }),
        map((tag: HashtagModel) => {
          if (tag && tag.id) {
            return true;
          } else {
            return this.router.parseUrl(`/not-found`);
          }
        }),
        first()
      );
  }

}
