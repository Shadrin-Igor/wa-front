import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable, of } from 'rxjs';
import { filter, first, map, switchMap } from 'rxjs/operators';
import { CategoryFieldNames, CategoryModel } from '@wt/models/category';
import { CategoriesFacade } from '@wt/core/+state/categories';


@Injectable()
export class LoadCategoryGuard implements CanActivate {

  NOT_FOUND_SLUG = 'not-found';

  constructor(private categoriesFacade: CategoriesFacade,
              private router: Router
  ) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    const url = state.url;
    const patern = /^(?:\/)((.*)+)$/gi;
    const params = patern.exec(url);
    const slug = params[1];

    if (slug !== this.NOT_FOUND_SLUG) {

      const where = {
        slug
      };
      const fields = {
        fields: [
          CategoryFieldNames.NAME,
          CategoryFieldNames.DESCRIPTION,
          CategoryFieldNames.SLUG,
          CategoryFieldNames.IMAGE
        ]
      };
      this.categoriesFacade.loadCategories(where, fields, 1);

      return this.categoriesFacade.waitLoading()
        .pipe(
          filter(item => !!item),
          switchMap(() => {
            return this.categoriesFacade.getCategory(slug);
          }),
          map((category: CategoryModel) => {
            if (category && category.id) {
              return true;
            }
            return this.router.parseUrl(`/not-found`);
          }),
          first()
        );
    } else {
      return of(false);
    }
  }

}
