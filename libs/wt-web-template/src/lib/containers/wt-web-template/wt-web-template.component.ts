import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit
} from '@angular/core';
import { filter, first } from 'rxjs/operators';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { untilComponentDestroyed } from '@wt/core/utils';
import { CategoriesFacade } from '@wt/core/+state/categories';
import { HashtagsFacade } from '@wt/core/+state/hashtags';

@Component({
  selector: 'wt-web-template',
  templateUrl: './wt-web-template.component.html',
  styleUrls: ['./wt-web-template.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WtWebTemplateComponent implements OnInit, OnDestroy {
  categoryId: number;
  tagId: number;
  mainTitle = 'Web applications';
  pageData = {
    title: this.mainTitle,
    subTitle: '',
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temp or incididunt ut labore et dolore magna aliqua. Ut enim ad minim.',
    image: ''
  };
  loaded = false;

  constructor(private categoriesFacade: CategoriesFacade,
              private hashtagsFacade: HashtagsFacade,
              private route: ActivatedRoute,
              private router: Router,
              private cd: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.route.params
      .pipe(
        untilComponentDestroyed(this)
      )
      .subscribe((params: Params) => {
        const slug = this.getUrl(params.slug);
        const isTag = params && params.slug && params.slug.indexOf('.html') !== -1;
        if (slug) {
          if (!isTag) {
            this.getCategory(slug);
          } else {
            this.getTag(slug);
          }
        }
      });
  }

  ngOnDestroy(): void {
  }

  getTag(slug) {
    return this.hashtagsFacade.getHashtags(slug)
      .pipe(
        filter(item => !!item && !!item.id),
        first(),
        untilComponentDestroyed(this)
      )
      .subscribe(tag => {
          this.pageData = {
            title: tag.name,
            subTitle: this.mainTitle,
            description: tag.description,
            image: tag.image
          };
          this.tagId = tag.id;
          this.loaded = true;
          this.cd.detectChanges();
        }
      );
  }

  getCategory(slug) {
    return this.categoriesFacade.getCategory(slug)
      .pipe(
        filter(item => !!item && !!item.id),
        first(),
        untilComponentDestroyed(this)
      )
      .subscribe(category => {
          this.pageData = {
            title: category.name,
            subTitle: this.mainTitle,
            description: category.description,
            image: category.image
          };
          this.categoryId = category.id;
          this.loaded = true;
          this.cd.detectChanges();
        }
      );
  }

  getUrl(slug) {
    if (slug) {
      const patern = /^(?:\/)?((?:[\d\w-]*)+)(?:\.html)?$/gi;
      const params = patern.exec(slug);
      return params[1];
    }
  }
}
