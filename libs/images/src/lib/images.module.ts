import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';


import { ClipboardModule } from 'ngx-clipboard';

import { FuseSharedModule } from '@wt/fuse';
import { DialogsModule } from '@wt/core/modules/dialogs/dialogs.module';

import { containers } from './containers';
import { FilesUploadModule } from '@wt/core/modules/files-upload/files-upload.module';
import { CKEditorModule } from 'ckeditor4-angular';
import { InputTagsModule } from '@wt/core/components/input-tags/input-tags.module';
// import { NgrxFormsModule } from 'ngrx-forms';
import { FormFieldErrorModule } from '@wt/core/components/form-field-error/form-field-error.module';
import { SpinnerModule } from '@wt/core/components/spinner/spinner.module';
import { TableMenuModule } from '@wt/core/components/table-menu/table-menu.module';
import { ButtonModule } from '@wt/core/components/button/button.module';
import { ImagesRoutingModule } from './images-routing.module';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatStepperModule } from '@angular/material/stepper';
import { MatDialogModule } from '@angular/material/dialog';
import { AutoFormModule } from '@wt/core/components/auto-form/auto-form.module';
import { AutoFormService } from '@wt/core/services/auto-form/auto-form.service';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ImagesRoutingModule,

    // Material
    MatTableModule,
    MatPaginatorModule,
    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,

    MatSelectModule,
    MatStepperModule,

    FuseSharedModule,
    MatDialogModule,
    DialogsModule,
    // CoreModule,

    FilesUploadModule,
    CKEditorModule,
    InputTagsModule,

    FormFieldErrorModule,
    SpinnerModule,
    TableMenuModule,
    ButtonModule,

    ClipboardModule,
    AutoFormModule
  ],
  declarations: [...containers],
  providers: [AutoFormService]
})
export class ImagesModule {
}
