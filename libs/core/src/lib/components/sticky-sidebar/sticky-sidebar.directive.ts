import {
  AfterContentInit,
  ContentChild,
  Directive,
  ElementRef,
  forwardRef,
  Inject,
  InjectionToken,
  NgZone,
  OnDestroy,
  OnInit,
  Optional,
  PLATFORM_ID,
  Renderer2
} from '@angular/core';
import { fromEvent, merge, Subject, Subscription } from 'rxjs';
import * as elementResizeDetectorMaker from 'element-resize-detector';
import { Platform } from '@angular/cdk/platform';
import { isPlatformBrowser } from '@angular/common';

export interface StickySidebarOptions {
  topSpacing: number;

  bottomSpacing: number;

  stickyClass: string;

  resizeSensor: boolean;

  minWidth: boolean | number;

  containerSelector: string;

  containerForWidth: boolean | string;
}

export const defaultStickySidebarOptions: StickySidebarOptions = {
  topSpacing: 0,
  bottomSpacing: 0,
  stickyClass: 'is-affixed',
  resizeSensor: true,
  minWidth: false,
  containerSelector: '.container',
  containerForWidth: false
};


export const STICKY_SIDEBAR_OPTIONS: InjectionToken<StickySidebarOptions> = new InjectionToken<StickySidebarOptions>('STICKY_SIDEBAR_OPTIONS');

@Directive({
  selector: '[wtStickySidebar]'
})
export class StickySidebarDirective implements OnInit, AfterContentInit, OnDestroy {
  @ContentChild(forwardRef(() => StickySidebarInnerDirective), { static: true, read: ElementRef })
  private sidebarInner: ElementRef;


  private eventSub: Subscription;
  private sidebarEl: HTMLElement;
  private containerForWidth: HTMLElement;
  private affixedType = 'STATIC';
  private direction = 'down';
  private support = {
    transform: false,
    transform3d: false
  };
  private _initialized = false;
  private _reStyle = false;
  private _breakpoint = false;
  // Dimensions of sidebar, container and screen viewport.
  private dimensions = {
    translateY: 0,
    maxTranslateY: 0,
    topSpacing: 0,
    lastTopSpacing: 0,
    bottomSpacing: 0,
    lastBottomSpacing: 0,
    sidebarHeight: 0,
    sidebarWidth: 0,
    containerTop: 0,
    containerHeight: 0,
    containerBottom: 0,
    viewportHeight: 0,
    viewportTop: 0,
    lastViewportTop: 0,
    sidebarLeft: 0,
    viewportBottom: 0,
    viewportLeft: 0
  };
  private container: HTMLElement;
  private _running: boolean;
  private scrollDirection: string;
  private lastScrollTop = 0;

  private resizeDetector: any;
  private _updateStickyListener: any;

  private options: StickySidebarOptions;

  private resize = new Subject<any>();
  private resizeStart: boolean;

  constructor(
    private element: ElementRef,
    @Optional() @Inject(STICKY_SIDEBAR_OPTIONS) private _options: StickySidebarOptions,
    private zone: NgZone,
    private renderer: Renderer2,
    @Inject(PLATFORM_ID) private platform: Platform
  ) {
    this.options = _options && { ...defaultStickySidebarOptions, ..._options } || defaultStickySidebarOptions;
    if (isPlatformBrowser(platform)) {
      this.sidebarEl = this.element.nativeElement as HTMLElement;

      this._setSupportFeatures();

      this.bindEvents();
    }
  }

  static offsetRelative(element: HTMLElement) {
    const result = { left: 0, top: 0 };
    let el = element;
    do {
      const offsetTop = el.offsetTop;
      const offsetLeft = el.offsetLeft;

      if (!isNaN(offsetTop)) result.top += offsetTop;

      if (!isNaN(offsetLeft)) result.left += offsetLeft;

      el =
        'BODY' === element.tagName
          ? (el.parentElement as HTMLElement)
          : (el.offsetParent as HTMLElement);
    } while (el);
    return result;
  }

  ngOnInit(): void {
  }

  bindEvents() {
    this.resizeDetector = elementResizeDetectorMaker();

    this.zone.runOutsideAngular(() => {
      this.eventSub = merge(fromEvent(window, 'scroll'), fromEvent(window, 'resize')).subscribe(
        evt => {
          if (evt.type === 'scroll') {
            const scrollTop = this.getScrollTop();
            this.scrollDirection = scrollTop > this.lastScrollTop ? 'down' : 'top';
            this.lastScrollTop = scrollTop;
          }
          this.updateSticky(evt);
        }
      );
    });
  }

  ngOnDestroy(): void {
    if (!isPlatformBrowser(this.platform)) return;

    if (this.eventSub)
      this.eventSub.unsubscribe();
    if (this.sidebarInner && this.sidebarInner.nativeElement)
      this.removeResizeEventListener(this.sidebarInner.nativeElement);
    if (this.container)
      this.removeResizeEventListener(this.container);
  }

  ngAfterContentInit(): void {
    if (!isPlatformBrowser(this.platform)) return;

    if (this.options.containerSelector) {
      let containers = document.querySelectorAll(this.options.containerSelector);
      containers = Array.prototype.slice.call(containers);

      containers.forEach((container, item) => {
        if (!container.contains(this.element.nativeElement)) return;
        this.container = container as HTMLElement;
      });

      if (!containers.length) throw new Error('The container does not contains on the sidebar.');
    }

    if(this.options.containerForWidth) {
      let containersForWidth = document.querySelectorAll(this.options.containerForWidth as string);
      containersForWidth = Array.prototype.slice.call(containersForWidth);

      containersForWidth.forEach((containerW, item) => {
        if (!containerW.contains(this.element.nativeElement)) return;
        this.containerForWidth = containerW as HTMLElement;
      });

      if (!containersForWidth.length) throw new Error('The container does not contains on the sidebar.');
    }


    this._widthBreakpoint();

    this.calcDimensions();

    this.stickyPosition();

    this._updateStickyListener = this.handleResizeEvent.bind(this);
    this.addResizeEventListener(this.sidebarInner.nativeElement, this._updateStickyListener);
    this.addResizeEventListener(this.container, this._updateStickyListener);
    this.zone.runOutsideAngular(() => {
      this.eventSub.add(this.resize.subscribe((evt) => {
        this.updateSticky(evt);
      }));
    });

  }

  handleResizeEvent(evt) {
    this.direction = 'down';
    this.resize.next(evt);
  }

  addResizeEventListener(element: HTMLElement, handler: Function) {
    this.resizeDetector.listenTo(element, handler);
  }

  removeResizeEventListener(element: HTMLElement) {
    if (this.resizeDetector)
      this.resizeDetector.uninstall(element);
  }

  calcDimensions() {
    if (this._breakpoint) return;
    const dims = this.dimensions;

    // Container of sticky sidebar dimensions.
    dims.containerTop = StickySidebarDirective.offsetRelative(this.container).top;
    dims.containerHeight = this.container.clientHeight;
    dims.containerBottom = dims.containerTop + dims.containerHeight;

    // Sidebar dimensions.
    dims.sidebarHeight = this.sidebarInner.nativeElement.offsetHeight;
    dims.sidebarWidth = this.options.containerForWidth ?  this.containerForWidth.offsetWidth : this.sidebarInner.nativeElement.offsetWidth;

    // Screen viewport dimensions.
    dims.viewportHeight = window.innerHeight;

    // Maximum sidebar translate Y.
    dims.maxTranslateY = dims.containerHeight - dims.sidebarHeight;

    this._calcDimensionsWithScroll();
  }

  _calcDimensionsWithScroll() {
    const dims = this.dimensions;

    dims.sidebarLeft = StickySidebarDirective.offsetRelative(this.sidebarEl).left;

    dims.viewportTop = this.getScrollTop();
    dims.viewportBottom = dims.viewportTop + dims.viewportHeight;
    dims.viewportLeft = document.documentElement.scrollLeft || document.body.scrollLeft;

    dims.topSpacing = this.options.topSpacing;
    dims.bottomSpacing = this.options.bottomSpacing;

    if ('VIEWPORT-TOP' === this.affixedType) {
      if (dims.topSpacing < dims.lastTopSpacing) {
        dims.translateY += dims.lastTopSpacing - dims.topSpacing;
        this._reStyle = true;
      }
    } else if ('VIEWPORT-BOTTOM' === this.affixedType) {
      // Adjust translate Y in the case decrease bottom spacing value.
      if (dims.bottomSpacing < dims.lastBottomSpacing) {
        dims.translateY += dims.lastBottomSpacing - dims.bottomSpacing;
        this._reStyle = true;
      }
    }

    dims.lastTopSpacing = dims.topSpacing;
    dims.lastBottomSpacing = dims.bottomSpacing;
  }

  isSidebarFitsViewport() {
    const dims = this.dimensions;
    const offset = this.scrollDirection === 'down' ? dims.lastBottomSpacing : dims.lastTopSpacing;
    return this.dimensions.sidebarHeight + offset < this.dimensions.viewportHeight;
  }

  observeScrollDir() {
    const dims = this.dimensions;
    if (dims.lastViewportTop === dims.viewportTop) return;

    const furthest = 'down' === this.direction ? Math.min : Math.max;

    if (dims.viewportTop === furthest(dims.viewportTop, dims.lastViewportTop))
      this.direction = 'down' === this.direction ? 'up' : 'down';
  }

  getAffixType() {
    this._calcDimensionsWithScroll();
    const dims = this.dimensions;
    const colliderTop = dims.viewportTop + dims.topSpacing;
    let affixType = this.affixedType;


    if (colliderTop <= dims.containerTop || dims.containerHeight <= dims.sidebarHeight) {
      dims.translateY = 0;
      affixType = 'STATIC';
    } else {
      affixType =
        'up' === this.direction
          ? this._getAffixTypeScrollingUp()
          : this._getAffixTypeScrollingDown();
    }

    dims.translateY = Math.max(0, dims.translateY);
    dims.translateY = Math.min(dims.containerHeight, dims.translateY);
    dims.translateY = Math.round(dims.translateY);

    dims.lastViewportTop = dims.viewportTop;
    return affixType;
  }

  _getAffixTypeScrollingDown() {
    const dims = this.dimensions;
    const sidebarBottom = dims.sidebarHeight + dims.containerTop;
    const colliderTop = dims.viewportTop + dims.topSpacing;
    const colliderBottom = dims.viewportBottom - dims.bottomSpacing;
    let affixType = this.affixedType;

    if (this.isSidebarFitsViewport()) {
      if (dims.sidebarHeight + colliderTop >= dims.containerBottom) {
        dims.translateY = dims.containerBottom - sidebarBottom;
        affixType = 'CONTAINER-BOTTOM';
      } else if (colliderTop >= dims.containerTop) {
        dims.translateY = colliderTop - dims.containerTop;
        affixType = 'VIEWPORT-TOP';
      }
    } else {
      if (dims.containerBottom <= colliderBottom) {
        dims.translateY = dims.containerBottom - sidebarBottom;
        affixType = 'CONTAINER-BOTTOM';
      } else if (sidebarBottom + dims.translateY <= colliderBottom) {
        dims.translateY = colliderBottom - sidebarBottom;
        affixType = 'VIEWPORT-BOTTOM';
      } else if (
        dims.containerTop + dims.translateY <= colliderTop &&
        (0 !== dims.translateY && dims.maxTranslateY !== dims.translateY)
      ) {
        affixType = 'VIEWPORT-UNBOTTOM';
      }
    }

    return affixType;
  }

  _getAffixTypeScrollingUp() {
    const dims = this.dimensions;
    const sidebarBottom = dims.sidebarHeight + dims.containerTop;
    const colliderTop = dims.viewportTop + dims.topSpacing;
    const colliderBottom = dims.viewportBottom - dims.bottomSpacing;
    let affixType = this.affixedType;

    if (colliderTop <= dims.translateY + dims.containerTop) {
      dims.translateY = colliderTop - dims.containerTop;
      affixType = 'VIEWPORT-TOP';
    } else if (dims.containerBottom <= colliderBottom) {
      dims.translateY = dims.containerBottom - sidebarBottom;
      affixType = 'CONTAINER-BOTTOM';
    } else if (!this.isSidebarFitsViewport()) {
      if (
        dims.containerTop <= colliderTop &&
        (0 !== dims.translateY && dims.maxTranslateY !== dims.translateY)
      ) {
        affixType = 'VIEWPORT-UNBOTTOM';
      }
    }

    return affixType;
  }

  _getStyle(affixType) {
    if ('undefined' === typeof affixType) return;

    const style = { inner: {}, outer: {} };
    const dims = this.dimensions;

    switch (affixType) {
      case 'VIEWPORT-TOP':
        style.inner = {
          position: 'fixed',
          top: dims.topSpacing,
          left: dims.sidebarLeft - dims.viewportLeft,
          width: dims.sidebarWidth
        };
        break;
      case 'VIEWPORT-BOTTOM':
        style.inner = {
          position: 'fixed',
          top: 'auto',
          left: dims.sidebarLeft,
          bottom: dims.bottomSpacing,
          width: dims.sidebarWidth
        };
        break;
      case 'CONTAINER-BOTTOM':
      case 'VIEWPORT-UNBOTTOM':
        const translate = this._getTranslate('0', dims.translateY + 'px');

        if (translate) style.inner = { transform: translate };
        else style.inner = { position: 'absolute', top: dims.translateY, width: dims.sidebarWidth };
        break;
    }

    switch (affixType) {
      case 'VIEWPORT-TOP':
      case 'VIEWPORT-BOTTOM':
      case 'VIEWPORT-UNBOTTOM':
      case 'CONTAINER-BOTTOM':
        style.outer = { height: dims.sidebarHeight, position: 'relative' };
        break;
    }

    style.outer = { height: '', position: '', ...style.outer };
    style.inner = {
      position: 'relative',
      top: '',
      left: '',
      bottom: '',
      width: '',
      transform: '',
      ...style.inner
    };

    return style;
  }

  stickyPosition(force: boolean = false) {
    if (this._breakpoint) return;

    force = this._reStyle || force || false;

    const offsetTop = this.options.topSpacing;
    const offsetBottom = this.options.bottomSpacing;

    const affixType = this.getAffixType();
    const style = this._getStyle(affixType);

    const innerEl = this.sidebarInner.nativeElement as HTMLElement;

    if ((this.affixedType !== affixType || force) && affixType) {
      //const affixEvent = 'affix.' + affixType.toLowerCase().replace('viewport-', '') + StickySidebarDirective.EVENT_KEY;
      //StickySidebar.eventTrigger(this.sidebar, affixEvent);

      if ('STATIC' === affixType)
        this.renderer.removeClass(this.sidebarEl, this.options.stickyClass);
      else this.renderer.addClass(this.sidebarEl, this.options.stickyClass);

      Object.keys(style.outer).forEach(key => {
        const unit = 'number' === typeof style.outer[key] ? 'px' : '';
        this.sidebarEl.style[key] = style.outer[key] + unit;
      });

      Object.keys(style.inner).forEach(key => {
        const unit = 'number' === typeof style.inner[key] ? 'px' : '';
        innerEl.style[key] = style.inner[key] + unit;
      });

      //const affixedEvent = 'affixed.'+ affixType.toLowerCase().replace('viewport-', '') + EVENT_KEY;
      //StickySidebar.eventTrigger(this.sidebar, affixedEvent);
    } else {
      if (this._initialized) innerEl.style.left = style.inner['left'];
    }

    this.affixedType = affixType;
  }

  _widthBreakpoint() {
    if (window.innerWidth <= this.options.minWidth) {
      this._breakpoint = true;
      this.affixedType = 'STATIC';

      this.renderer.removeAttribute(this.sidebarEl, 'style');
      this.renderer.removeAttribute(this.sidebarInner.nativeElement, 'style');
      this.renderer.removeClass(this.sidebarEl, this.options.stickyClass);
    } else {
      this._breakpoint = false;
    }
  }

  updateSticky(event: Event) {
    if (this._running) return;
    this._running = true;

    (eventType => {
      requestAnimationFrame(() => {
        switch (eventType) {
          case 'scroll':
            this._calcDimensionsWithScroll();
            this.observeScrollDir();
            this.stickyPosition();
            break;

          // When browser is resizing or there's no event, observe width
          // breakpoint and re-calculate dimensions.
          case 'resize':
          default:
            this._widthBreakpoint();
            this.calcDimensions();
            this.stickyPosition(true);
            break;
        }
        this._running = false;
      });
    })(event.type);
  }

  _setSupportFeatures() {
    const support = this.support;

    support.transform = this.supportTransform();
    support.transform3d = this.supportTransform(true);
  }

  _getTranslate(y = '0', x = '0', z = '0') {
    if (this.support.transform3d) return 'translate3d(' + y + ', ' + x + ', ' + z + ')';
    else if (this.support.transform) return 'translate(' + y + ', ' + x + ')';
    else return false;
  }

  supportTransform(transform3d = false) {
    let result = null;
    const property = transform3d ? 'perspective' : 'transform',
      upper = property.charAt(0).toUpperCase() + property.slice(1),
      prefixes = ['Webkit', 'Moz', 'O', 'ms'],
      support = document.createElement('support'),
      style = support.style;

    (property + ' ' + prefixes.join(upper + ' ') + upper).split(' ').forEach(function(property, i) {
      if (style[property] !== undefined) {
        result = property;
        return false;
      }
    });
    return result;
  }

  private getScrollTop() {
    return document.documentElement.scrollTop || document.body.scrollTop || document.documentElement.offsetTop * -1;
  }
}

@Directive({
  selector: '[wtStickySidebarInner]'
})
export class StickySidebarInnerDirective {
  constructor() {
  }
}
