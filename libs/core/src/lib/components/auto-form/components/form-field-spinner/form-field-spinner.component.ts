import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'shared-form-field-spinner',
  templateUrl: './form-field-spinner.component.html',
  styleUrls: ['./form-field-spinner.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormFieldSpinnerComponent {
}
