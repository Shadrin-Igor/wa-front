import {
  AfterViewInit,
  ChangeDetectionStrategy, ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { fromEvent, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, takeUntil } from 'rxjs/operators';
import { IFormField } from '@wt/core/components/auto-form/interfaces/form-field';
import { InputType } from '@wt/core/components/auto-form/interfaces/input-type';
import { IFormFieldFormat } from '@wt/core/components/auto-form/interfaces/form-field-format';

@Component({
  selector: 'shared-form-field-input',
  templateUrl: './form-field-input.component.html',
  styleUrls: ['./form-field-input.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormFieldInputComponent implements OnInit, OnDestroy, AfterViewInit {
  @Input() fieldData: IFormField;
  @Input() form: FormGroup;

  @ViewChild('input', { static: false }) input: ElementRef<HTMLInputElement>;

  pendingStatus = 'PENDING';
  unSubscribeSubject$ = new Subject();
  toolTipMessage = '';

  constructor(private cd: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.checkInputType();
    this.checkInputMask();
  }

  ngOnDestroy() {
    this.unSubscribeSubject$.next();
    this.unSubscribeSubject$.complete();
  }

  ngAfterViewInit() {
    const control = this.form.get(this.fieldData.field);

    if (control.value) {
      this.input.nativeElement.value = control.value;
    }

    this.form.get(this.fieldData.field).statusChanges
      .pipe(
        distinctUntilChanged(),
        takeUntil(this.unSubscribeSubject$)
      )
      .subscribe(() => {
        this.cd.markForCheck();
      });

    fromEvent(this.input.nativeElement, 'input')
      .pipe(
        map(() => this.input.nativeElement.value.trim()),
        distinctUntilChanged(),
        debounceTime(300),
        takeUntil(this.unSubscribeSubject$)
      )
      .subscribe((value: string) => {
        control.markAsTouched();
        control.markAsDirty();
        control.setValue(value);
      });
  }

  inputType: InputType = InputType.Text;
  inputMask = null;
  placeholder = '';

  private checkInputType(): void {
    switch (this.fieldData.format) {
      case IFormFieldFormat.Email:
        this.inputType = InputType.Email;
        break;
      case IFormFieldFormat.Phone:
        this.inputType = InputType.Tel;
        break;
      case IFormFieldFormat.Number:
        this.inputType = InputType.Number;
        break;
      default:
        this.inputType = InputType.Text;
    }
  }

  private checkInputMask(): void {
    switch (this.fieldData.format) {
      case IFormFieldFormat.Phone:
        this.inputMask = '+(0000)000-00-00';
        this.placeholder = '+(0000)000-00-00';
        break;
    }
  }
}
