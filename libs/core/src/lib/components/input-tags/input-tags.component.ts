import {Component, OnDestroy, OnInit} from '@angular/core';
import {HashtagsFacade} from '@wt/core/+state/hashtags';
import {untilComponentDestroyed} from '@wt/core/utils';

interface Tag {
  id: number;
  title: string;
}

@Component({
  selector: 'wt-input-tags',
  templateUrl: './input-tags.component.html',
  styleUrls: ['./input-tags.component.scss']
})
export class InputTagsComponent implements OnInit {
  allTags: Tag[] = [
    {id: 1, title: 'Apple'},
    {id: 2, title: 'Lemon'},
    {id: 3, title: 'Lime'},
    {id: 4, title: 'Orange'},
    {id: 5, title: 'Strawberry'}
  ];
  tags: Tag[] = [{id: 2, title: 'Lemon'}];

  constructor() { }

  ngOnInit() {
  }

  changedText(tags) {
  }

  changedList(tags) {
  }
}
