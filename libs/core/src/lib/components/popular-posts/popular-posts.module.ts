import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PopularPostsComponent } from './popular-posts.component';
import {SpinnerModule} from '@wt/core/components/spinner/spinner.module';
import {DateFormatModule} from '@wt/core/pipes/date-format/date-format.module';
import {RouterModule} from '@angular/router';
import { ImagesHelperService } from '@wt/core/services/images/images-helper.service';

@NgModule({
  declarations: [PopularPostsComponent],
  imports: [
    CommonModule,
    SpinnerModule,
    DateFormatModule,
    RouterModule
  ],
  exports: [PopularPostsComponent],
  providers: [ImagesHelperService]
})
export class PopularPostsModule { }
