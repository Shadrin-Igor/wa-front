import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostCategoriesComponent } from './post-categories.component';
import {SpinnerModule} from '@wt/core/components/spinner/spinner.module';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [PostCategoriesComponent],
  imports: [
    CommonModule,
    SpinnerModule,
    RouterModule
  ],
  exports: [PostCategoriesComponent]
})
export class PostCategoriesModule { }
