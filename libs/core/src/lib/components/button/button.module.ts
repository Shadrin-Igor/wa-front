import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ButtonComponent} from '@wt/core/components/button/button.component';
import {SpinnerModule} from '@wt/core/components/spinner/spinner.module';

@NgModule({
  declarations: [ButtonComponent],
  imports: [
    CommonModule,
    SpinnerModule
  ],
  exports: [ButtonComponent]
})
export class ButtonModule { }
