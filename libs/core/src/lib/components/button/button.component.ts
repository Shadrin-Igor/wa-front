import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'wt-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css'],
  animations: [
    trigger('buttonState', [
      state('normal', style({
        width: 'auto'
      })),
      state('loading', style({
        width: '50px'
      })),
      transition('normal => loading', animate('250ms ease-in')),
      transition('loading => normal', animate('250ms ease-out'))
    ])
  ]
})
export class ButtonComponent implements OnInit, OnChanges {
  @Input() loading = false;
  @Input() buttonType = 'button';
  @Input() title: string;
  @Input() color: string;
  @Input() buttonStyle = 'raised-button';
  @Input() disabled = false;

  state: 'normal' | 'loading' = 'normal';

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.loading) {
      this.state =  changes.loading.currentValue ? 'loading' : 'normal';
    }
  }
}
