import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';

export interface IBreadCrumpsItem {
  title: string;
  url?: string;
}

@Component({
  selector: 'wt-bread-crumps',
  templateUrl: './bread-crumps.component.html',
  styleUrls: ['./bread-crumps.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BreadCrumpsComponent implements OnInit {
  @Input() list: IBreadCrumpsItem;
  constructor() { }

  ngOnInit() {
  }

}
