import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'wt-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() title = 'Web applications';
  @Input() subTitle: string;
  @Input() description: string;
  @Input() image: string;
  @Input() small = false;
  constructor() { }

  ngOnInit() {
  }

}
