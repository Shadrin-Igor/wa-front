import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input, OnChanges,
  OnInit,
  Output, SimpleChanges
} from '@angular/core';

@Component({
  selector: 'wt-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImageComponent implements OnInit, OnChanges {
  @Input() versions: { [key: number]: string };
  @Input() version: number;
  @Input() title: string;
  @Input() index: number;
  @Input() haveZoom = false;
  @Output() clickEvent = new EventEmitter<number>();
  src: string;

  constructor() {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('changes', changes);
    if (changes.versions && changes.versions.currentValue) {
      this.src = changes.versions.currentValue[this.version];
    }
  }

  clickHandler() {
    this.clickEvent.emit(this.index);
  }
}
