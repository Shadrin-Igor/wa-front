/*import { setValue, StateUpdateFns } from 'ngrx-forms';
import { ValidationErrors } from 'ngrx-forms/src/state';
import { email, maxLength, minLength, pattern } from 'ngrx-forms/validation';
import slugify from 'slugify';*/

import { FORMS_GUID_VALIDATION_REGEX, FORMS_USERNAME_VALIDATION_REGEXP } from './rules';
import { FormValue } from '@wt/core/models';

export const emailValidation = []; // [email];
export const passwordValidation = []; // [minLength(6), maxLength(72)];
export const usernameValidation = []; // [pattern(FORMS_USERNAME_VALIDATION_REGEXP), minLength(4)];

export function entityGuidValidate<T extends string | null | undefined>(
  value: T
): any {
  if (value === null || value === undefined || value.length === 0) {
    return {};
  }
  if (FORMS_GUID_VALIDATION_REGEX.test(value as string)) {
    return {};
  }
  return {
    pattern: {
      pattern: FORMS_GUID_VALIDATION_REGEX.toString(),
      actual: value
    }
  };
}

export function emptyTextValidate<T extends string | null | undefined>(value: T): any {
  if (
    value === null ||
    value === undefined ||
    value.length === 0 ||
    value.replace(/\s*/g, '').length === 0
  ) {
    return {
      required: {
        actual: value
      }
    };
  }
  return {};
}

export function emptyNumberValidate<T extends number | null | undefined>(value: T): any {
  if (
    value === null ||
    value === undefined ||
    value === 0
  ) {
    return {
      required: {
        actual: value
      }
    };
  }
  return {};
}

export function emptyArrayValidate<T extends (number | string)[] | null | undefined>(value: T): any {
  if (
    !value ||
    value.length === 0
  ) {
    return {
      required: {
        actual: value
      }
    };
  }
  return {};
}

/*export const slugValidationRule: StateUpdateFns<FormValue> = {
  slug: (state, parentState) => {
    if (!parentState.value.slug) {
      return setValue(parentState.controls.slug, slugify(parentState.value.title).toLowerCase());
    }
    return state;
  }
};*/
