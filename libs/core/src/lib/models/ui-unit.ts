export interface UiUnit {
  name: string;
  type?: string;
  loaded?: boolean;
  error?: {
    status: number;
    error:
      | {
          source: string;
          title: string;
        }[]
      | string;
  };
}
