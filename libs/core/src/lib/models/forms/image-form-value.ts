import {FormValue} from './form-value';

export interface ImageFormValue extends FormValue {
  title: string;
  categoryId: number;
  tagIds: string;
  description: string;
  metaTitle: string;
  metaDescription: string;
  metaKeyWord: string;
}
