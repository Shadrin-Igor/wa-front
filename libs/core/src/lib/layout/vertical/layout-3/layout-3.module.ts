import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseSidebarModule } from '@fuse/components/index';
import { FuseSharedModule } from '@fuse/shared.module';
import { VerticalLayout3Component } from '@wt/core/layout/vertical/layout-3/layout-3.component';
import { ContentModule } from '@wt/core/layout/components/content/content.module';
import { ChatPanelModule } from '@wt/core/layout/components/chat-panel/chat-panel.module';
import { NavbarModule } from '@wt/core/layout/components/navbar/navbar.module';
import { QuickPanelModule } from '@wt/core/layout/components/quick-panel/quick-panel.module';
import { FooterModule } from '@wt/core/layout/components/footer/footer.module';
import { ToolbarModule } from '@wt/core/layout/components/toolbar/toolbar.module';

@NgModule({
    declarations: [
        VerticalLayout3Component
    ],
    imports     : [
        RouterModule,

        FuseSharedModule,
        FuseSidebarModule,

        ChatPanelModule,
        ContentModule,
        FooterModule,
        NavbarModule,
        QuickPanelModule,
        ToolbarModule
    ],
    exports     : [
        VerticalLayout3Component
    ]
})
export class VerticalLayout3Module
{
}
