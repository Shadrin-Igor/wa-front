import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
  {
    id: 'applications',
    title: 'Разделы',
    translate: 'NAV.APPLICATIONS',
    type: 'group',
    icon: 'apps',
    children: [
      {
        id: 'images',
        title: 'Посты',
        translate: 'NAV.IMAGES',
        type: 'collapsable',
        icon: 'dashboard',
        children: [
          {
            id: 'images.list',
            title: 'Все посты',
            translate: 'NAV.IMAGES',
            icon: 'collections',
            type: 'item',
            url: '/images/list'
          },
          {
            id: 'images.add',
            title: 'Добавить пост',
            translate: 'NAV.ADD-IMAGES',
            icon: 'add_photo_alternate',
            type: 'item',
            url: '/images/new'
          }
        ]
      }
    ]
  },
];
