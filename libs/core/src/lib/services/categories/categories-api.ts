import {BaseAPI} from '../base/base-api';
import {
  CategoriesEndpointTypes,
  CategoriesRequestConfig,
  CategoriesResponse
} from '@wt/models/endpoints';

import {Request} from '@wt/models/http';
import {CategoriesLoadRequest} from '@wt/models/endpoints/categories/categories-load/request/categories-load-request';
import {CategoriesLoadRequestConfig} from '@wt/models/endpoints/categories/categories-load/request/categories-load-request-config';
import {CategoriesLoadResponseFactory} from '@wt/models/endpoints/categories/categories-load/response/categories-load-response-factory';

export class CategoriesApi extends BaseAPI {
  constructor(protected domain: string) {
    super(domain);
  }

  createRequest(paramsConfig: CategoriesRequestConfig, endPointType: string) {
    let request: Request;
    switch (endPointType) {
      case CategoriesEndpointTypes.LOAD:
        request = new CategoriesLoadRequest(this.domain, <CategoriesLoadRequestConfig>paramsConfig);
        break;
      default:
        throw new Error(`Request for ${endPointType} is undefined `);
    }
    return request;
  }

  handleResponse(response: any, endPointType: string) {
    let courseResponse: CategoriesResponse;
    switch (endPointType) {
      case CategoriesEndpointTypes.LOAD:
        courseResponse = CategoriesLoadResponseFactory.createResponse(response);
        break;
      default:
        throw new Error(`Response for ${endPointType} is undefined `);
    }
    return courseResponse;
  }
}
