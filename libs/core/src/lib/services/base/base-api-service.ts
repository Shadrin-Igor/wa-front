import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { HttpClientResult } from '../../models';
import { BaseAPI } from './base-api';
import { PostRequest, PutRequest, Request, RequestResultInfo, Response } from '@wt/models/http';

export abstract class BaseAPIService {

  public static OK = 'OK';
  public static ERROR = 'ERROR';

  constructor(protected httpClient: HttpClient) {
  }

  protected api: BaseAPI;

  public doUpload(paramsConfig, endPoint: string): Observable<HttpClientResult<Response>> {
    const request = this.api.createRequest(paramsConfig, endPoint) as PutRequest;
    const headers = new HttpHeaders({ ...request.headers });
    const req = new HttpRequest(request.httpMethod, request.url, request.body, { headers, reportProgress: true });
    return this.httpClient
      .request(req)
      .pipe(
        switchMap((response) => {
          return of(this.handleResponse(response, endPoint));
        }),
        catchError(response => {
          const errorResult: HttpClientResult<any> = this.handleResponse(response, endPoint);
          if (!errorResult.info.isValid) {
            return throwError(errorResult);
          } else {
            return of(errorResult);
          }
        })
      );
  }

  public doRequest(paramsConfig, endPoint: string): Observable<HttpClientResult<Response>> {
    const request = this.api.createRequest(paramsConfig, endPoint);
    const params = this.prepareRequestParams(request);

    return this.httpClient
      .request(params.httpMethod, params.url, params.options)
      .pipe(
        map((response: any) => {
          return this.handleResponse(response, endPoint);
        }),
        catchError(response => {
          const errorResult: HttpClientResult<any> = this.handleResponse(response, endPoint);
          if (!errorResult.info.isValid) {
            return throwError(errorResult);
          } else {
            return of(errorResult);
          }
        })
      );
  }

  async _doRequest(paramsConfig, endPoint: string): Promise<HttpClientResult<Response>> {
    const request = this.api.createRequest(paramsConfig, endPoint);
    const params = this.prepareRequestParams(request);
    const result: HttpClientResult<Response> = await this.httpClient
      .request(params.httpMethod, params.url, params.options)
      .pipe(
        map((response: any) => {
          return this.handleResponse(response, endPoint);
        }),
        catchError(response => {
          const errorResult: HttpClientResult<any> = this.handleResponse(response, endPoint);
          if (!errorResult.info.isValid) {
            return throwError(errorResult);
          } else {
            return of(errorResult);
          }
        })
      ).toPromise();
    return result;
  }

  protected prepareRequestParams(
    request: Request
  ): { httpMethod; url; options } {
    const httpMethod = request.httpMethod.toString();
    const { url, headers, params } = request;

    const options = {
      body: undefined,
      headers: { ...headers },
      params: { ...params },
      observe: 'response'
    };

    if (request instanceof PostRequest || request instanceof PutRequest) {
      options.body = { ...request.body };
    }

    return { httpMethod, url, options };
  }

  public handleResponse(response: any, endPoint: string): HttpClientResult<Response | any> {
    const apiResponse = this.api.handleResponse(response, endPoint);

    let clientResult: HttpClientResult<Response>;

    if (apiResponse) {
      clientResult = {
        info: <RequestResultInfo>{
          isValid: true,
          message: BaseAPIService.OK
        },
        data: apiResponse
      };
    } else {
      clientResult = {
        info: <RequestResultInfo>{
          isValid: false,
          message: BaseAPIService.ERROR
        },
        data: response
      };
    }
    return clientResult;
  }
}
