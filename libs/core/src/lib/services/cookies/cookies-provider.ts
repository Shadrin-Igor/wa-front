import { isPlatformBrowser } from '@angular/common';
import { Injector, PLATFORM_ID, Provider } from '@angular/core';

import { BrowserCookiesService } from './browser/browser-cookies.service';
import { CookiesService } from './cookies.service';
// import { ServerCookiesService } from './server/server-cookies.service';

export function cookiesProviderFactory(platformId: object, injector: Injector): CookiesService {
    // if (isPlatformBrowser(platformId)) {
        return new BrowserCookiesService(injector)
    // }
    // return new ServerCookiesService(injector);
}

export const CookiesProvider: Provider = {
    provide: CookiesService,
    deps: [PLATFORM_ID, Injector],
    useFactory: cookiesProviderFactory,
};
