import { BaseAPI } from '../base/base-api';
import {
  FilesDeleteRequest,
  FilesDeleteRequestConfig,
  FilesDeleteResponseFactory,
  FilesEndpointTypes,
  FilesLoadRequest,
  FilesLoadRequestConfig,
  FilesRequestConfig,
  FilesResponse,
  FilesSortRequest,
  FilesSortRequestConfig,
  FilesSortResponseFactory
} from '@wt/models/endpoints';

import { Request } from '@wt/models/http';
import { FilesUpdateRequest } from '@wt/models/endpoints/files/files-update/request/files-update-request';
import { FilesUpdateRequestConfig } from '@wt/models/endpoints/files/files-update/request/files-update-request-config';
import { FilesUpdateResponseFactory } from '@wt/models/endpoints/files/files-update/response/files-update-response-factory';

export class FilesApi extends BaseAPI {
  constructor(protected domain: string) {
    super(domain);
  }

  createRequest(paramsConfig: FilesRequestConfig, endPointType: string) {
    let request: Request;
    switch (endPointType) {
      case FilesEndpointTypes.UPDATE:
        request = new FilesUpdateRequest(this.domain, <FilesUpdateRequestConfig>paramsConfig);
        break;
      case FilesEndpointTypes.DELETE:
        request = new FilesDeleteRequest(this.domain, <FilesDeleteRequestConfig>paramsConfig);
        break;
      case FilesEndpointTypes.SORT:
        request = new FilesSortRequest(this.domain, <FilesSortRequestConfig>paramsConfig);
        break;
      case FilesEndpointTypes.LOAD:
        request = new FilesLoadRequest(this.domain, <FilesLoadRequestConfig>paramsConfig);
        break;
      default:
        throw new Error(`Request for ${endPointType} is undefined `);
    }
    return request;
  }

  handleResponse(response: any, endPointType: string) {
    let courseResponse: FilesResponse;
    switch (endPointType) {
      case FilesEndpointTypes.UPDATE:
        courseResponse = FilesUpdateResponseFactory.createResponse(response);
        break;
      case FilesEndpointTypes.DELETE:
        courseResponse = FilesDeleteResponseFactory.createResponse(response);
        break;
      case FilesEndpointTypes.SORT:
        courseResponse = FilesSortResponseFactory.createResponse(response);
        break;
      case FilesEndpointTypes.LOAD:
        courseResponse = FilesSortResponseFactory.createResponse(response);
        break;
      default:
        throw new Error(`Response for ${endPointType} is undefined `);
    }
    return courseResponse;
  }
}
