import { createFeatureSelector, createSelector } from '@ngrx/store';

import { IMAGES_FEATURE_KEY, ImagesState } from './images.reducer';
import { ImageModel } from '@wt/models/image';
import { CategoriesState, getCategoriesState } from '@wt/core/+state/categories';
import { getHashtagsState, HashtagsState } from '@wt/core/+state/hashtags';

// Lookup the 'Images' feature state managed by NgRx
const getImagesState = createFeatureSelector<ImagesState>(IMAGES_FEATURE_KEY);

const getLoaded = createSelector(
  getImagesState,
  (state: ImagesState) => {
    return state.loaded;
  }
);

const getError = createSelector(
  getImagesState,
  (state: ImagesState) => state.error
);

const geSelected = createSelector(
  getImagesState,
  (state: ImagesState) => state.selected
);

const getImages = createSelector(
  getImagesState,
  getLoaded,
  (state: ImagesState, isLoaded) => {
    return isLoaded ? state.list : null;
  }
);

const getFullImages = createSelector(
  getImagesState,
  getCategoriesState,
  getHashtagsState,
  (images: ImagesState, categories: CategoriesState, hashtagsState: HashtagsState) => {
    return images.list.map((image: ImageModel) => {
      const newItem = { ...image };
      if (image.categoryId && categories.ids && categories.ids.indexOf(image.categoryId) !== -1) {
        const finCategory = categories.list.find(item => item.id === image.categoryId);
        newItem['category'] = { ...finCategory };
      }

      if (image.tagIds && image.tagIds.length) {
        newItem['tags'] = hashtagsState.list.filter(item => image.tagIds.indexOf(item.id) !== -1);
      }

      return { ...newItem };
    });
  }
);

const getFullImagesByCategory = (categoryId: number) => createSelector(
  getFullImages,
  (images: ImageModel[]) => {
    return images.filter((item: ImageModel) => item.categoryId === categoryId);
  }
);

const getFullImagesByTag = (tagId: number) => createSelector(
  getFullImages,
  (images: ImageModel[]) => {
    return images.filter((item: ImageModel) => item.tagIds && item.tagIds.length && item.tagIds.indexOf(tagId) !== -1);
  }
);

const getFullImage = (id: number | string) => createSelector(
  getImages,
  getCategoriesState,
  getHashtagsState,
  (list: ImageModel[], categories: CategoriesState, hashtagsState: HashtagsState) => {
    if (list && list.length) {
      const imageObj = list.find((item) => item.id === id || item.slug === id);

      let image;
      if (imageObj) {
        image = { ...imageObj };

        if (image) {
          if (image.categoryId) {
            const category = categories.list.find(item => item.id === image.categoryId);
            if (category) {
              image['category'] = {...category};
            }
          }

          if (hashtagsState && hashtagsState.list && hashtagsState.list.length && image.tagIds) {
            image['tags'] = hashtagsState.list.map(item => ({...item}))
              .filter((item) => image.tagIds.includes(item.id))
              .map(item => ({
                name: item.name,
                count: item.count,
                slug: item.slug
              }));
          }

          if (image.prevImageId) {
            const searchImage = list.find((item) => item.id === image.prevImageId);
            if (searchImage) {
              image['prevImage'] = {...searchImage};

              if (image.categoryId) {
                const category = categories.list.find(item => item.id === image.categoryId);
                if (category) {
                  image['prevImage']['category'] = category;
                }
              }
            }
          }

          if (image.nextImageId) {
            const searchImage = list.find((item) => item.id === image.nextImageId);
            if (image) {
              image['nextImage'] = {...searchImage};

              if (image.categoryId) {
                const category = categories.list.find(item => item.id === image.categoryId);
                if (category && image['nextImage']) {
                  image['nextImage']['category'] = category;
                }
              }
            }
          }
          return image;
        }
      }
      return null;
    }
  }
);

const getImage = (id: number | string) => createSelector(
  getImages,
  (list: ImageModel[]) => {
    if (list && list.length) {
      const result = list.find((item) => item.id === id || item.slug === id);
      if (result) {
        return { ...result };
      }
    }
    return null;
  }
);
/*
const getImagesState = createSelector(
  getImagesState,
  getLoaded,
  (state: ImagesState, isLoaded) => {
    console.log('state', state);
    return isLoaded ? state : null;
  }
);*/

export const imagesQuery = {
  getLoaded,
  getError,
  getImages,
  geSelected,
  getImage,
  getFullImages,
  getFullImage,
  getFullImagesByCategory,
  getFullImagesByTag
};
