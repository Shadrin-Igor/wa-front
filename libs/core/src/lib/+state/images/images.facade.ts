import { Injectable } from '@angular/core';
import { filter, first, switchMap } from 'rxjs/operators';
import { select, Store } from '@ngrx/store';
import {
  ClearImages,
  GetImage,
  GetImageNext,
  ImageCreate,
  ImagesDelete,
  ImagesLoad,
  ImagesUpdate
} from '@wt/core/+state/images/images.actions';
import { ImagesStateData } from '@wt/core/+state/images/images.reducer';
import { imagesQuery } from '@wt/core/+state/images/images.selectors';
import { ImageFieldNames, ImageFullModel, ImageModel } from '@wt/models/image';
import { FieldsModel } from '@wt/models/fields';
import { PagingType } from '@wt/models/entity';
import { Observable } from 'rxjs';
import { FileFieldNames } from '@wt/models/file';
import { CategoryFieldNames } from '@wt/models/category';
import { HashtagFieldNames } from '@wt/models/hashtag';

@Injectable()
export class ImagesFacade {
  fields = {
    fields: [
      ImageFieldNames.TITLE,
      ImageFieldNames.DESCRIPTION,
      ImageFieldNames.DESCRIPTION_FULL,
      ImageFieldNames.CATEGORY_ID,
      ImageFieldNames.IS_PUBLIC,
      ImageFieldNames.SLUG,
      ImageFieldNames.VIEWS,
      ImageFieldNames.META_DESCRIPTION,
      ImageFieldNames.META_KEYWORD,
      ImageFieldNames.META_TITLE
    ],
    files: {
      fields: ['file', 'folder'],
      versions: {}
    }
  };

  fields2: FieldsModel = {
    fields: [
      ImageFieldNames.TITLE,
      ImageFieldNames.DESCRIPTION,
      ImageFieldNames.DESCRIPTION_FULL,
      ImageFieldNames.CATEGORY_ID,
      ImageFieldNames.IS_PUBLIC,
      ImageFieldNames.SLUG,
      ImageFieldNames.VIEWS,
      ImageFieldNames.META_DESCRIPTION,
      ImageFieldNames.META_KEYWORD,
      ImageFieldNames.META_TITLE
    ],
    files: {
      fields: [
        FileFieldNames.FILE,
        FileFieldNames.FOLDER,
        FileFieldNames.TITLE,
        FileFieldNames.MAIN,
        FileFieldNames.ORDER
      ],
      versions: {}
    },
    tags: {
      fields: [HashtagFieldNames.NAME, HashtagFieldNames.SLUG],
      count: {}
    },
    category: {
      fields: [CategoryFieldNames.NAME, CategoryFieldNames.SLUG]
    }
  };

  loaded$ = this.store.pipe(select(imagesQuery.getLoaded));
  getImages$ = this.store.pipe(select(imagesQuery.getImages));
  getFullImages$ = this.store.pipe(select(imagesQuery.getFullImages));
  selected$ = this.store.pipe(select(imagesQuery.geSelected));

  constructor(private store: Store<ImagesStateData>) {
  }

  getImageByCategory(categoryId: number) {
    return this.store.pipe(select(imagesQuery.getFullImagesByCategory(categoryId)));
  }

  getImageByTag(tagId: number) {
    return this.store.pipe(select(imagesQuery.getFullImagesByTag(tagId)));
  }

  // loadImages(where = {}, fields = {}, paging?: PagingType, sort?: string, imagesType?: 'popular' | '') {
  loadImages(data: {
    where?,
    fields,
    paging?: PagingType,
    sort?: string,
    imagesType?: 'popular' | '',
    tagId?: number
    forPanel?: boolean
  }) {
    const { where, fields, paging, sort, imagesType, tagId, forPanel } = data;
    this.store.dispatch(ImagesLoad({ where, fields, paging, sort, imagesType, tagId, forPanel }));
  }

  imagesCreateOrUpdate(params: ImageModel) {
    if (!params.id) {
      this.store.dispatch(ImageCreate(params));
    } else {
      this.store.dispatch(ImagesUpdate(params));
    }
  }

  waitLoading() {
    return this.store.pipe(
      select(imagesQuery.getLoaded),
      filter((item) => !!item)
    );
  }

  imagesDelete(itemId: number) {
    this.store.dispatch(ImagesDelete({ itemId }));
  }

  loadImage(idOrSlug: number | string, fields: any = [], forPanel = false) {
    this.store.dispatch(GetImage({ id: idOrSlug, fields, forPanel }));
  }

  getLoadImage(id: number, fields: FieldsModel, forPanel = false) {
    this.loadImage(id, fields, forPanel);
    return this.store.pipe(
      select(imagesQuery.geSelected),
      filter((item) => !!item),
      first(),
      /*      tap((selectedId) => {
              this.loadImage(id, fields, forPanel);
            }),*/
      switchMap(() => {
        return this.waitLoading();
      }),
      switchMap(() => {
        return this.getImage(id);
      })
    );
  }

  getImage(id: number | string): Observable<ImageModel> {
    return this.store.pipe(select(imagesQuery.getImage(id)));
  }

  loadImageNext(id: number, fields?: any) {
    return this.store.dispatch(GetImageNext({ id, fields }));
  }

  getFullImage(id: number | string): Observable<ImageFullModel> {
    return this.store.pipe(select(imagesQuery.getFullImage(id)));
  }

  clearImages() {
    return this.store.dispatch(ClearImages());
  }
}
