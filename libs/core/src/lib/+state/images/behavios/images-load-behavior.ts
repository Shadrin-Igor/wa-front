import {Behavior} from '@wt/core/+state/behavior';
import {HttpClientResult} from '@wt/core/models';
import {SetLastRequestResult} from '@wt/core/+state/last-request-result';
import {ImagesLoad200, ImagesLoadResponse} from '@wt/models/endpoints';
import {ResponseStatus} from '@wt/models/http';
import {ImageModel} from '@wt/models/image';
import {CategoryModelData, HashtagModelData, ImageModelData} from '@wt/models/data';
import {
  ImagesLoad,
  ImagesLoadFail,
  ImagesLoadSuccess
} from '@wt/core/+state/images/images.actions';
import {CategoryModel} from '@wt/models/category';
import {
  CategoriesLoadSuccess,
  CategoriesLoadSuccessPayload
} from '@wt/core/+state/categories/categories.actions';
import {HashtagModel} from '@wt/models/hashtag';
import {
  HashtagsLoadSuccess,
  HashtagsLoadSuccessPayload
} from '@wt/core/+state/hashtags/hashtags.actions';
import { Action } from '@ngrx/store';

export class ImagesLoadBehavior extends Behavior {
  constructor(
    protected action: any,
    protected result: HttpClientResult<ImagesLoadResponse>
  ) {
    super(action);
  }

  resolve(): Array<Action> {
    const status = this.result.data.status;
    const categories: CategoryModel[] = [];
    let tags: HashtagModel[] = [];
    const correlationParams = this.action.correlationParams;
    let returnedActions: Array<Action> = [];
    const setLastRequestResponseAction = this.generateSetLastRequestResult(
      this.action.type,
      this.result.info,
      this.action.correlationParams
    );
    switch (status) {
      case ResponseStatus.STATUS_200: {
        const response: ImagesLoad200 = this.result.data;
        const {data, meta} = response.body;
        if (data) {
          const imagesData: ImageModel[] = this.modelAdapter.convertItems<ImageModel, ImageModelData>(ImageModel, data);

          returnedActions = [
            ImagesLoadSuccess({images: imagesData, meta})
          ];

          data.forEach(item => {
            if (item.tags) {
              const hashtagsData: HashtagModel[] = this.modelAdapter.convertItems<HashtagModel, HashtagModelData>(HashtagModel, item.tags);
              tags = [...tags, ...hashtagsData];

            }

            if (item.category) {
              const category: CategoryModel = this.modelAdapter.convertItem<CategoryModel, CategoryModelData>(CategoryModel, item.category);
              const existsCategory = categories.find(item => item.id === category.id);
              if (!existsCategory) {
                categories.push(category);
              }
            }
          });

          if (tags && tags.length) {
            const tagsParams: HashtagsLoadSuccessPayload = {
              hashtags: tags
            };
            returnedActions.push(HashtagsLoadSuccess(tagsParams));
          }

          if (categories && categories.length) {
            const categoryParams: CategoriesLoadSuccessPayload = {
              categories
            };
            returnedActions.push(CategoriesLoadSuccess(categoryParams));
          }
        }

        break;
      }
      case ResponseStatus.STATUS_400:
      case ResponseStatus.STATUS_404:
      case ResponseStatus.STATUS_422:
      case ResponseStatus.STATUS_401:
        const errorResponse = this.result.data;
        const error = errorResponse.body.errors ? errorResponse.body.errors[0].title : '';
        returnedActions = [
          ImagesLoadFail({error})
        ];
        break;
      default: {
        returnedActions = [
          ImagesLoadFail({error: 'Unhandled error'})
        ];
      }

    }
    return [setLastRequestResponseAction, ...returnedActions];
  }
}
