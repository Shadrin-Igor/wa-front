import { Behavior } from '@wt/core/+state/behavior';
import { HttpClientResult } from '@wt/core/models';
import { ImagesGet200, ImagesGetResponse } from '@wt/models/endpoints/images';
import { ResponseStatus } from '@wt/models/http';
import { ImageModel } from '@wt/models/image';
import { CategoryModelData, HashtagModelData, ImageModelData } from '@wt/models/data';
import { GetImageFail, GetImageSuccess } from '@wt/core/+state/images/images.actions';
import { FilesLoadSuccess } from '@wt/core/+state/files';
import { CategoryModel } from '@wt/models/category';
import { CategoriesLoadSuccess } from '@wt/core/+state/categories/categories.actions';
import { HashtagModel } from '@wt/models/hashtag';
import { HashtagsLoadSuccess, HashtagsLoadSuccessPayload } from '@wt/core/+state/hashtags/hashtags.actions';
import { Action } from '@ngrx/store';

export class GetImageBehavior extends Behavior {
  constructor(
    protected action: any,
    protected result: HttpClientResult<ImagesGetResponse>
  ) {
    super(action);
  }

  resolve(): Array<Action> {
    const status = this.result.data.status;
    const correlationParams = this.action.correlationParams;

    let returnedActions: Array<Action>;
    const setLastRequestResponseAction = this.generateSetLastRequestResult(
      this.action.type,
      this.result.info,
      this.action.correlationParams
    );
    switch (status) {
      case ResponseStatus.STATUS_200: {
        const response: ImagesGet200 = this.result.data;
        const { data } = response.body;
        const imageData: ImageModel = this.modelAdapter.convertItem<ImageModel, ImageModelData>(ImageModel, data);

        returnedActions = [
          GetImageSuccess({ image: { ...imageData } })
        ];

        if (imageData.files) {
          returnedActions.push(FilesLoadSuccess({ files: imageData.files }));
        }

        if (data.tags && data.tags.length) {
          const hashtags = this.modelAdapter.convertItems<HashtagModel, HashtagModelData>(HashtagModel, data.tags);
          const tagsParams: HashtagsLoadSuccessPayload = {
            hashtags
          };
          returnedActions.push(HashtagsLoadSuccess(tagsParams));
        }

        if (data.category) {
          const category: CategoryModel = this.modelAdapter.convertItem<CategoryModel, CategoryModelData>(CategoryModel, data.category);
          if (category) {
            returnedActions.push(CategoriesLoadSuccess({ categories: [category] }));
          }
        }
        break;
      }
      case ResponseStatus.STATUS_400:
      case ResponseStatus.STATUS_404:
      case ResponseStatus.STATUS_422:
      case ResponseStatus.STATUS_401:
        const errorResponse = this.result.data;
        const error = errorResponse.body.errors[0].title;
        returnedActions = [
          GetImageFail({ error })
        ];
        break;
      default: {
        returnedActions = [
          GetImageFail({ error: 'Unhandled error' })
        ];
      }

    }
    return [setLastRequestResponseAction, ...returnedActions];
  }
}
