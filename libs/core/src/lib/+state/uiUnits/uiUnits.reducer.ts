import { Action, createReducer, on } from '@ngrx/store';

import { UiUnitItem } from '@wt/models/ui-units/uiUnitItem';
import * as uiUnitsActions from './uiUnits.actions';
import { ReducerService } from '@wt/core/services/reducer/reducer.service';

export const UIUNITS_FEATURE_KEY = 'uiunits';

export interface UiUnitsState {
  list: UiUnitItem[];
  ids: string[];
}

export interface UiUnitsStateData {
  uiunits: UiUnitsState;
}
/*
export interface UiUnitsPartialState {
  readonly [UIUNITS_FEATURE_KEY]: UiUnitsStateData;
}*/

export const initialUiUnitsState = {
  list: [],
  ids: []
};

const newReducer = createReducer(
  initialUiUnitsState,
  on(uiUnitsActions.UiUnitsSet, (state, payload) => {
    return {
      ...ReducerService.upsertOne(state, {
        data: payload.data,
        id: payload.id
      })
    };
  })
);

export function uiUnitsReducer(state: UiUnitsState | undefined, action: Action) {
  return newReducer(state, action);
}

/*export function uiUnitsReducer(
  state: UiUnitsState = initialUiUnitsState,
  action: UiUnitsAction
): UiUnitsState {
  switch (action.type) {
    case UiUnitsActionTypes.UiUnitsSet: {
      state = {
        ...ReducerService.upsertOne(state, {data: action.payload.data, id: action.payload.id})
      };
      break;
    }
  }
  return state;
}*/
