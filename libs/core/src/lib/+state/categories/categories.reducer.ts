import { ReducerService } from '@wt/core/services/reducer/reducer.service';
import { CategoryModel } from '@wt/models/category';
import * as categoriesActions from '@wt/core/+state/categories/categories.actions';
import { Action, createReducer, on } from '@ngrx/store';

export const CATEGORIES_FEATURE_KEY = 'categories';

export interface CategoriesState {
  list: CategoryModel[];
  ids: number[];
  selected: number;
  loaded: boolean; // has the Categories list been loaded
  error?: any; // last none error (if any)
}


export interface CategoriesStateData {
  [CATEGORIES_FEATURE_KEY]: CategoriesState;
}
/*
export interface CategoriesPartialState {
  readonly [CATEGORIES_FEATURE_KEY]: CategoriesStateData;
}*/

export const initialCategoryState = {
  list: [],
  ids: [],
  selected: null,
  loaded: false
};

const categoriesReducer = createReducer(
  initialCategoryState,
  on(categoriesActions.CategoriesLoad, state => ({ ...state, loaded: false })),
  on(categoriesActions.CategoriesLoadSuccess, (state, payload) => ({
    ...ReducerService.upsertMeny(state, payload.categories),
    loaded: true
  }))
);

export function categoryReducer(state: CategoriesState | undefined, action: Action) {
  return categoriesReducer(state, action);
}

/*
export function categoriesReducer(
  state: CategoriesState = initialCategoryState,
  action: CategoriesAction
): CategoriesState {

  switch (action.type) {
    case CategoriesActionTypes.CategoriesLoad: {
      state = {
        ...state,
        loaded: false
      };
      break;
    }
    case CategoriesActionTypes.CategoriesLoadSuccess: {
      if (action.payload.categories.length) {
        state = {
          ...ReducerService.upsertMeny(state, action.payload.categories),
          loaded: true
        };
      }

      break;
    }
  }
  return state;
}
*/
