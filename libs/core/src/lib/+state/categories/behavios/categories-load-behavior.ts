import { Behavior } from '@wt/core/+state/behavior';
import { HttpClientResult } from '@wt/core/models';
import { ResponseStatus } from '@wt/models/http';
import { CategoryModelData } from '@wt/models/data';
import { CategoriesLoadFail, CategoriesLoadSuccess } from '@wt/core/+state/categories/categories.actions';
import { CategoriesLoad200, CategoriesLoadResponse } from '@wt/models/endpoints';
import { CategoryModel } from '@wt/models/category';
import { Action } from '@ngrx/store';

export class CategoriesLoadBehavior extends Behavior {
  constructor(
    protected action: any,
    protected result: HttpClientResult<CategoriesLoadResponse>
  ) {
    super(action);
  }

  resolve(): Array<Action> {
    const status = this.result.data.status;
    let returnedActions: Array<Action>;
    const setLastRequestResponseAction = this.generateSetLastRequestResult(
      this.action.type,
      this.result.info,
      this.action.correlationParams
    );
    switch (status) {
      case ResponseStatus.STATUS_200: {
        const response: CategoriesLoad200 = this.result.data;
        const { data, meta } = response.body;
        const categoriesData: CategoryModel[] = this.modelAdapter.convertItems<CategoryModel, CategoryModelData>(CategoryModel, data);

        returnedActions = [
          CategoriesLoadSuccess({ categories: categoriesData, meta })
        ];

        break;
      }
      case ResponseStatus.STATUS_400:
      case ResponseStatus.STATUS_404:
      case ResponseStatus.STATUS_422:
      case ResponseStatus.STATUS_401:
        const errorResponse = this.result.data;
        const error = errorResponse.body.errors ? errorResponse.body.errors[0].title : '';
        returnedActions = [
          CategoriesLoadFail({ error })
        ];
        break;
      default: {
        returnedActions = [
          CategoriesLoadFail({ error: 'Unhandled error' })
        ];
      }

    }
    return [setLastRequestResponseAction, ...returnedActions];
  }
}
