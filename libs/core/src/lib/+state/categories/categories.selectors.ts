import { createFeatureSelector, createSelector } from '@ngrx/store';

import { CATEGORIES_FEATURE_KEY, CategoriesState } from './categories.reducer';
import { CategoryModel } from '@wt/models/category/categoryModel';

// Lookup the 'Categories' feature state managed by NgRx
export const getCategoriesState = createFeatureSelector<CategoriesState>(CATEGORIES_FEATURE_KEY);

const getLoaded = createSelector(
  getCategoriesState,
  (state: CategoriesState) => {
    return state.loaded;
  }
);

const getError = createSelector(
  getCategoriesState,
  (state: CategoriesState) => state.error
);

const geSelected = createSelector(
  getCategoriesState,
  (state: CategoriesState) => state.selected
);

const getCategories = createSelector(
  getCategoriesState,
  getLoaded,
  (state: CategoriesState, isLoaded) => {
    return isLoaded ? state.list : null;
  }
);

const getCategory = (slugOrId: number | string) => createSelector(
  getCategories,
  (list: CategoryModel[]) => {
    if (list && list.length) {
      return list.find((item) => item.id === slugOrId || item.slug === slugOrId);
    }
  }
);
/*
const getCategoriesState = createSelector(
  getCategoriesState,
  getLoaded,
  (state: CategoriesState, isLoaded) => {
    console.log('state', state);
    return isLoaded ? state : null;
  }
);*/

export const categoriesQuery = {
  getLoaded,
  getError,
  getCategories,
  geSelected,
  getCategory
};
