import { Injectable } from '@angular/core';
import { Actions } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { CoreState } from '@wt/core/+state/core.reducer';
import { BaseEffects } from '@wt/core/+state/base.effects';

@Injectable()
export class MainEffects extends BaseEffects {

  constructor(
    protected actions$: Actions,
    protected store: Store<CoreState>
  ) {
    super(actions$, store);
  }
}
