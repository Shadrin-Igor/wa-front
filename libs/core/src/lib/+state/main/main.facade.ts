import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { mainQuery } from './main.selectors';
import { MainStateData } from '@wt/core/+state/main/main.reducer';

@Injectable()
export class MainFacade {
  constructor(private store: Store<MainStateData>) {
  }

  getMetadata() {
    return this.store.pipe(select(mainQuery.getMetaData));
  }
}
