import { MetaDataModel } from '@wt/core/models';
import * as mainActions from './main.actions';
import { Action, createReducer, on } from '@ngrx/store';

export const MAIN_FEATURE_KEY = 'main';

export interface MainState {
  metaData: MetaDataModel
}

export const initialMainState = {
  metaData: null
};

export interface MainStateData {
  main: MainState;
}

/*
export interface MainPartialState {
  readonly [MAIN_FEATURE_KEY]: MainStateData;
}

export const initialMainState = {
  metaData: {}
};
*/

const newReducer = createReducer(
  initialMainState,
  on(mainActions.UpdateMetaData, (state, payload) => {
    const metaData: MetaDataModel = payload
    return {
      ...state,
      metaData
    };
  })
);

export function mainReducer(state: MainState | undefined, action: Action) {
  return newReducer(state, action);
}

/*
export function mainReducer(
  state: MainState = initialMainState,
  action: MainAction
): MainState {
  switch (action.type) {
    case MainActionTypes.UpdateMetaData: {
      const metaData: MetaDataModel = action.payload
      state = {
        ...state,
        metaData
      };
      break;
    }
  }
  return state;
}
*/
