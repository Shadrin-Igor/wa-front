import { createAction, props } from '@ngrx/store';
import { MetaDataModel } from '@wt/core/models';

export const UpdateMetaData = createAction(
  '[MetaData] Update MetaData',
  props<MetaDataModel>()
);

export const UpdateMetaDataSuccess = createAction(
  '[MetaData] Update MetaData Success'
);

export const UpdateMetaDataFail = createAction(
  '[MetaData] Update MetaData Error',
  props<{ error }>()
);

/*
import {
  AggregatableAction,
  CorrelationParams,
  FailActionForAggregation
} from '@wt/core/+state/aggregate';

import { MetaDataModel } from '@wt/core/models';

export enum MainActionTypes {
  UpdateMetaData = '[MetaData] Update MetaData',
  UpdateMetaDataSuccess = '[MetaData] Update MetaData Success',
  UpdateMetaDataFail = '[MetaData] Update MetaData Error'
}

export interface MetaDataPayment extends MetaDataModel {
}

export class UpdateMetaData implements AggregatableAction {
  readonly type = MainActionTypes.UpdateMetaData;

  constructor(public payload: MetaDataPayment, public correlationParams: CorrelationParams) {
  }
}

export interface EmptyPayload {
}

export class UpdateMetaDataSuccess implements AggregatableAction {
  readonly type = MainActionTypes.UpdateMetaDataSuccess;

  constructor(public payload: EmptyPayload, public correlationParams: CorrelationParams) {
  }
}

export class UpdateMetaFail implements FailActionForAggregation {
  readonly type = MainActionTypes.UpdateMetaDataFail;

  constructor(public payload: EmptyPayload, public error: any, public correlationParams: CorrelationParams) {
  }
}

export type MainAction =
  | UpdateMetaData
  | UpdateMetaDataSuccess
  | UpdateMetaFail;
*/
