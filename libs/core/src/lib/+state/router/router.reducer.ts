/*
import {
  ROUTER_CANCEL,
  ROUTER_ERROR,
  ROUTER_NAVIGATION,
  RouterCancelAction,
  RouterErrorAction,
  RouterNavigationAction,
  SerializedRouterStateSnapshot
} from '@ngrx/router-store';

export const ROUTER_FEATURE_KEY = 'routers';

import { RouterActions } from './router.actions';

export interface RouterState  {
  readonly router: SerializedRouterStateSnapshot;
}

export const initialRouterData = { root: null, url: '' };

export function routerReducer(
  state = initialRouterData,
  action:
    | RouterActions
    | RouterNavigationAction<SerializedRouterStateSnapshot>
    | RouterCancelAction<SerializedRouterStateSnapshot>
    | RouterErrorAction<SerializedRouterStateSnapshot>
): any {
  switch (action.type) {
    case ROUTER_ERROR:
    case ROUTER_CANCEL:
    case ROUTER_NAVIGATION:
      return {
        ...state,
        ...action.payload.routerState
      };
    default:
      return state;
  }
}
*/
