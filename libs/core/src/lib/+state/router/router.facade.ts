import { Injectable } from '@angular/core';

import { select, Store } from '@ngrx/store';

import { Go } from '@wt/core/+state/router/router.actions';
import { CoreState } from '@wt/core/+state/core.reducer';

@Injectable()
export class RouterFacade {
  constructor(private store: Store<CoreState>) {}

  goTo(path, extras = null) {
    return this.store.dispatch(Go({path, extras}));
  }
}
