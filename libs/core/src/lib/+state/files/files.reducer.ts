import * as filesActions from './files.actions';
import { FileModel } from '@wt/models/file';
import { ReducerService } from '@wt/core/services/reducer/reducer.service';
import { Action, createReducer, on } from '@ngrx/store';

export const FILES_FEATURE_KEY = 'files';

export interface FilesState {
  list: FileModel[];
  ids: string[];
  selected: number;
  loading: boolean; // has the Files list been loading
  error?: any; // last none error (if any)
}


export interface FilesStateData {
  [FILES_FEATURE_KEY]: FilesState;
}
/*
export interface FilesPartialState {
  readonly [FILES_FEATURE_KEY]: FilesStateData;
}*/

export const initialFileState = {
  list: [],
  ids: [],
  selected: null,
  loading: false
};

const categoriesReducer = createReducer(
  initialFileState,
  on(filesActions.FilesLoadSuccess, (state, payload) => {
    const files: FileModel[] = payload.files;
    return {
      ...ReducerService.upsertMeny(state, files),
      loading: false
    };
  }),
  on(filesActions.FileDeleteSuccess, (state, payload) => ({
    ...ReducerService.deleteOne(state, payload.deleteId),
    loading: false
  })),
  on(filesActions.FilesLoad,
    filesActions.FileDelete,
    filesActions.FilesSort,(state, payload) => ({
      ...state,
      loading: true
  })),
  on(filesActions.FileUpdateSuccess, (state, payload) => {
    const files: FileModel[] = payload.files;
    const newState = ReducerService.upsertMeny(state, files);
    return {
      list: [...newState.list],
      ids: [...newState.ids],
      loading: false,
      selected: null
    };
  }),
  on(filesActions.FilesSortSuccess, (state, payload) => {
    const files: FileModel[] = payload.files;
    state = {
      ...ReducerService.upsertMeny(state, files),
      loading: true
    };
  }),
);

export function filesReducer(state: FilesState | undefined, action: Action) {
  return categoriesReducer(state, action);
}

/*
export function filesReducer(
  state: FilesState = initialFileState,
  action: FilesAction
): FilesState {
  switch (action.type) {
    case FilesActionTypes.FilesLoadSuccess: {
      const files: FileModel[] = action.payload.files;
      state = {
        ...ReducerService.upsertMeny(state, files),
        loading: false
      };
      break;
    }
    case FilesActionTypes.FileDeleteSuccess: {
      state = {
        ...ReducerService.deleteOne(state, action.payload.deleteId),
        loading: false
      };
      break;
    }
    case FilesActionTypes.FilesLoad:
    case FilesActionTypes.FileDelete:
    case FilesActionTypes.FilesSort: {
      state = {
        ...state,
        loading: true
      };
      break;
    }
    case FilesActionTypes.FileUpdateSuccess: {
      const files: FileModel[] = action.payload.files;
      const newState = ReducerService.upsertMeny(state, files);
      state = {
        list: [...newState.list],
        ids: [...newState.ids],
        loading: false,
        selected: null
      };
      break;
    }
    case FilesActionTypes.FilesSortSuccess: {
      const files: FileModel[] = action.payload.files;
      state = {
        ...ReducerService.upsertMeny(state, files),
        loading: true
      };
      break;
    }
  }
  return state;
}
*/
