import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { catchError, flatMap, map, mergeMap, switchMap, tap } from 'rxjs/operators';
import { Store } from '@ngrx/store';

import { HttpClientResult } from '@wt/core/models';
import { CoreState } from '@wt/core/+state/core.reducer';
import { BaseEffects } from '@wt/core/+state/base.effects';
import {
  LoginSend,
  LoginSendFail,
  LoginSendSuccess,
  RefreshToken,
  RefreshTokenFail,
  RefreshTokenSuccess
} from './users.actions';
import { LoginSendCreateRequestConfig, LoginSendCreateResponse } from '@wt/models/endpoints/auth/login-send';
import { LoginSendBehavior } from '@wt/core/+state/users/behavios/login-send-behavior';
import { UsersService } from '@wt/core/services/users/users.service';
import { AuthenticationService } from '@wt/core/services/auth/authentication.service';
import { RouterService } from '@wt/core/services/router/router.service';
import { RefreshTokenRequestConfig, RefreshTokenResponse } from '@wt/models/endpoints/auth/refresh-token';
import { RefreshTokenBehavior } from '@wt/core/+state/users/behavios/refresh-token-behavior';
import { of } from 'rxjs';

@Injectable()
export class UsersEffects extends BaseEffects {

  @Effect()
  loginSend$ = this.actions$.pipe(
    ofType(LoginSend),
    mergeMap((action) => {
      const { email, password, remember } = action;
      const config: LoginSendCreateRequestConfig = { email, password };
      return this.usersService.loginSend(config)
        .pipe(
          flatMap((r: HttpClientResult<LoginSendCreateResponse>) => new LoginSendBehavior(action, r).resolve())
        )
    }),
    catchError((error) => {
      return of(LoginSendFail(error));
    })
  );

  @Effect({ dispatch: false })
  loginSendSuccess$ = this.actions$.pipe(
    ofType(LoginSendSuccess),
    tap((action) => {
      this.authenticationService.saveAuthInfo(action.user);
      const redirectUrl = this.authenticationService.getRedirectToUrl();
      this.routerService.goTo(redirectUrl ? [redirectUrl] : '/');
    })
  );

  @Effect()
  refreshToken$ = this.actions$.pipe(
    ofType(RefreshToken),
    switchMap((action) => {
      const { refreshToken } = action;
      const config: RefreshTokenRequestConfig = { refreshToken };
      return this.usersService.refreshToken(config)
        .pipe(
          flatMap((r: HttpClientResult<RefreshTokenResponse>) => new RefreshTokenBehavior(action, r).resolve())
        )
    }),
    catchError((error) => {
      return of(RefreshTokenFail(error));
    })
  );

  @Effect({ dispatch: false })
  refreshTokenSuccess$ = this.actions$.pipe(
    ofType(RefreshTokenSuccess),
    tap((action) => {
      const { token, refreshToken, tokenExp } = action;
      this.authenticationService.saveRefreshToken(token, tokenExp, refreshToken);
    })
  );

  constructor(
    protected actions$: Actions,
    protected store: Store<CoreState>,
    protected usersService: UsersService,
    protected authenticationService: AuthenticationService,
    protected routerService: RouterService
  ) {
    super(actions$, store);
  }
}
