import { createAction, props } from '@ngrx/store';
import { UserModel } from '@wt/models/user';

export const LoadUsers = createAction(
  '[Users] Load Users'
);

export const UsersLoaded = createAction(
  '[Users] Users Loaded',
  props<any>()
);

export const UsersLoadError = createAction(
  '[Users] Users Load Error',
  props<{ error }>()
);


export interface LoginSendPayment {
  email: string,
  password: string,
  remember?: boolean
}

export const LoginSend = createAction(
  '[Users] Auth Send',
  props<LoginSendPayment>()
);

export interface LoginSendSuccessPayment {
  user: UserModel
}

export const LoginSendSuccess = createAction(
  '[Users] Auth Send Success',
  props<LoginSendSuccessPayment>()
);

export const LoginSendFail = createAction(
  '[Users] Auth Send Error',
  props<{ error }>()
);


export interface RefreshTokenPayment {
  refreshToken: string
}

export const RefreshToken = createAction(
  '[Users] Refresh Token',
  props<RefreshTokenPayment>()
);

export interface RefreshTokenSuccessPayment {
  token: string,
  refreshToken: string,
  tokenExp: number
}
export const RefreshTokenSuccess = createAction(
  '[Users] Refresh Token Success',
  props<RefreshTokenSuccessPayment>()
);

export const RefreshTokenFail = createAction(
  '[Users] Refresh TokenError',
  props<{error}>()
);


export const SetLoaded = createAction(
  '[Users] Set Loaded'
);


/*
import { Action } from '@ngrx/store';
import { AggregatableAction, CorrelationParams, FailActionForAggregation } from '@wt/core/+state/aggregate';
import { UserModel } from '@wt/models/user';

export enum UsersActionTypes {
  LoadUsers = '[Users] Load Users',
  UsersLoaded = '[Users] Users Loaded',
  UsersLoadError = '[Users] Users Load Error',

  LoginSend = '[Users] Auth Send',
  LoginSendSuccess = '[Users] Auth Send Success',
  LoginSendError = '[Users] Auth Send Error',

  RefreshToken = '[Users] Refresh Token',
  RefreshTokenSuccess = '[Users] Refresh Token Success',
  RefreshTokenError = '[Users] Refresh TokenError',

  SetLoaded = '[Users] Set Loaded'
}

export interface EmptyPayload {};

export class LoadUsers implements AggregatableAction {
  readonly type = UsersActionTypes.LoadUsers;
  constructor(public payload: EmptyPayload, public correlationParams: CorrelationParams) { }
}

export class UsersLoadFail implements FailActionForAggregation {
  readonly type = UsersActionTypes.UsersLoadError;
  constructor(public payload: EmptyPayload, public error: any, public correlationParams: CorrelationParams) { }
}

export class UsersLoaded implements AggregatableAction {
  readonly type = UsersActionTypes.UsersLoaded;
  constructor(public payload: any, public correlationParams: CorrelationParams) { }
}

export interface LoginSendPayment {
  email: string,
  password: string,
  remember?: boolean
}

export class LoginSend implements AggregatableAction {
  readonly type = UsersActionTypes.LoginSend;

  constructor(public payload: LoginSendPayment, public correlationParams: CorrelationParams) { }
}

export interface LoginSendSuccessPayment {
  user: UserModel
}

export class LoginSendSuccess implements AggregatableAction {
  readonly type = UsersActionTypes.LoginSendSuccess;
  constructor(public payload: LoginSendSuccessPayment, public correlationParams: CorrelationParams) { }
}

export class LoginSendFail implements FailActionForAggregation {
  readonly type = UsersActionTypes.LoginSendError;
  constructor(public payload: EmptyPayload, public error: any, public correlationParams: CorrelationParams) { }
}

export interface RefreshTokenPayment {
  refreshToken: string
}

export class RefreshToken implements AggregatableAction {
  readonly type = UsersActionTypes.RefreshToken;

  constructor(public payload: RefreshTokenPayment, public correlationParams: CorrelationParams) { }
}

export interface RefreshTokenSuccessPayment {
  token: string,
  refreshToken: string,
  tokenExp: number
}

export class RefreshTokenSuccess implements AggregatableAction {
  readonly type = UsersActionTypes.RefreshTokenSuccess;
  constructor(public payload: RefreshTokenSuccessPayment, public correlationParams: CorrelationParams) { }
}

export class RefreshTokenFail implements FailActionForAggregation {
  readonly type = UsersActionTypes.RefreshTokenError;
  constructor(public payload: EmptyPayload, public error: any, public correlationParams: CorrelationParams) { }
}

// export type UsersAction = LoadUsers | UsersLoaded | UsersLoadFail;

export class SetLoaded implements AggregatableAction {
  readonly type = UsersActionTypes.SetLoaded;

  constructor(public payload: {}, public correlationParams: CorrelationParams) { }
}

export type UsersAction =
  | LoadUsers
  | UsersLoaded
  | UsersLoadFail
  | LoginSend
  | LoginSendSuccess
  | LoginSendFail
  | RefreshToken
  | RefreshTokenSuccess
  | RefreshTokenFail
  | SetLoaded;
*/
