import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { CoreState } from '@wt/core/+state/core.reducer';
import { BaseEffects } from '@wt/core/+state/base.effects';
import { catchError, map, tap } from 'rxjs/operators';
import * as notificationsActions from './notifications.actions';
import { Toast, ToasterService } from 'angular2-toaster';
import { ImageCreateSuccess, ImagesUpdateFail, ImagesUpdateSuccess } from '@wt/core/+state/images/images.actions';
import { NotificationsFacade } from '@wt/core/+state/notifications/notifications.facade';
import { NotificationModel } from '@wt/models/notification';
import {
  FileDelete,
  FileDeleteFail,
  FileDeleteSuccess,
  FilesSortFail,
  FilesSortSuccess,
  FileUpdateFail,
  FileUpdateSuccess
} from '@wt/core/+state/files';
import { of } from 'rxjs';

@Injectable()
export class NotificationsEffects extends BaseEffects {

  @Effect({ dispatch: false })
  notificationAdd$ = this.actions$.pipe(
    ofType(notificationsActions.NotificationAdd),
    tap((action) => {
      const { text, title, type, timeout } = action.payload;
      const toast: Toast = {
        body: text,
        title,
        type,
        timeout
      };
      this.toasterService.pop(toast);
    }),
    catchError((error) => {
      return of(notificationsActions.NotificationAddFail(error));
    })
  );

  @Effect({ dispatch: false })
  fileDeleteSuccess$ = this.actions$.pipe(
    ofType(FileDeleteSuccess),
    tap((action) => {
      const notification: NotificationModel = {
        id: 5,
        type: 'success',
        text: 'File delete successfully',
        timeout: 2000
      };
      this.notificationsFacade.addNotifications(notification);
    }),
    catchError((error) => {
      return of(FileDeleteFail(error));
    })
  );

  @Effect({ dispatch: false })
  imageCreateSuccess$ = this.actions$.pipe(
    ofType(ImageCreateSuccess),
    tap((action) => {
      const notification: NotificationModel = {
        id: 5,
        type: 'success',
        text: 'Image created successfully',
        timeout: 2000
      };
      this.notificationsFacade.addNotifications(notification);
    }),
    catchError((error) => {
      return of(ImagesUpdateFail(error));
    })
  );

  @Effect({ dispatch: false })
  imagesUpdateSuccess$ = this.actions$.pipe(
    ofType(ImagesUpdateSuccess),
    tap((action) => {
      const notification: NotificationModel = {
        id: 5,
        type: 'success',
        text: 'Image updated successfully',
        timeout: 2000
      };
      this.notificationsFacade.addNotifications(notification);
    }),
    catchError((error) => {
      return of(ImagesUpdateFail(error));
    })
  );

  @Effect({ dispatch: false })
  fileUpdateSuccess$ = this.actions$.pipe(
    ofType(FileUpdateSuccess),
    tap((action) => {
      const notification: NotificationModel = {
        id: 5,
        type: 'success',
        text: 'File updated successfully',
        timeout: 2000
      };
      this.notificationsFacade.addNotifications(notification);
    }),
    catchError((error) => {
      return of(FileUpdateFail(error));
    })
  );

  @Effect({ dispatch: false })
  filesSortSuccess$ = this.actions$.pipe(
    ofType(FilesSortSuccess),
    tap((action) => {
      const notification: NotificationModel = {
        id: 5,
        type: 'success',
        text: 'Image sort save successfully',
        timeout: 2000
      };
      this.notificationsFacade.addNotifications(notification);
    }),
    catchError((error) => {
      return of(FilesSortFail(error));
    })
  );

  constructor(
    protected actions$: Actions,
    protected store: Store<CoreState>,
    protected toasterService: ToasterService,
    protected notificationsFacade: NotificationsFacade
  ) {
    super(actions$, store);
  }
}
