import { Behavior } from '@wt/core/+state/behavior';
import { HttpClientResult } from '@wt/core/models';
import { ResponseStatus } from '@wt/models/http';
import { HashtagModelData } from '@wt/models/data';
import { HashtagsLoadFail, HashtagsLoadSuccess } from '@wt/core/+state/hashtags/hashtags.actions';
import { HashtagsLoad200, HashtagsLoadResponse } from '@wt/models/endpoints';
import { HashtagModel } from '@wt/models/hashtag';
import { Action } from '@ngrx/store';

export class HashtagsLoadBehavior extends Behavior {
  constructor(
    protected action: any,
    protected result: HttpClientResult<HashtagsLoadResponse>
  ) {
    super(action);
  }

  resolve(): Array<Action> {
    const status = this.result.data.status;
    const correlationParams = this.action.correlationParams;
    let returnedActions: Array<Action>;
    const setLastRequestResponseAction = this.generateSetLastRequestResult(
      this.action.type,
      this.result.info,
      this.action.correlationParams
    );
    switch (status) {
      case ResponseStatus.STATUS_200: {
        const response: HashtagsLoad200 = this.result.data;
        const { data, meta } = response.body;
        const hashtagsData: HashtagModel[] = this.modelAdapter.convertItems<HashtagModel, HashtagModelData>(HashtagModel, data);

        returnedActions = [
          HashtagsLoadSuccess({ hashtags: hashtagsData, meta })
        ];

        break;
      }
      case ResponseStatus.STATUS_400:
      case ResponseStatus.STATUS_404:
      case ResponseStatus.STATUS_422:
      case ResponseStatus.STATUS_401:
        const errorResponse = this.result.data;
        const error = errorResponse.body.errors ? errorResponse.body.errors[0].title : '';
        returnedActions = [
          HashtagsLoadFail({ error })
        ];
        break;
      default: {
        returnedActions = [
          HashtagsLoadFail({ error: 'Unhandled error' })
        ];
      }

    }
    return [setLastRequestResponseAction, ...returnedActions];
  }
}
