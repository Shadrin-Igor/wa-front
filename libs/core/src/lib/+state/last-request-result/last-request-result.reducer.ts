import { Action, createReducer, on } from '@ngrx/store';
import * as lastRequestActions from './last-request-result.actions';

export interface LastRequestSate {
  actionType: string;
  isValid: boolean;
  message: string;
}

/*
export interface LastRequestResultState {
  readonly lastRequestResult: LastRequestResultData;
}
*/

export const initialState: LastRequestSate = {
  actionType: '',
  isValid: true,
  message: ''
};

const newReducer = createReducer(
  initialState,
  on(lastRequestActions.SetLastRequestResult, (state, payload) => {
    const lastRequestResult = payload.lastRequestResult;
    return { ...state, ...lastRequestResult };
  })
);

export function reducer(state: LastRequestSate | undefined, action: Action) {
  return newReducer(state, action);
}

/*export function lastRequestResultReducer(
  state = initialState,
  action: LastRequestResultActions
): LastRequestResultData {
  switch (action.type) {
    case LastRequestResultActionTypes.SetLastRequestResult:
      const lastRequestResult = action.payload.lastRequestResult;
      return { ...state, ...lastRequestResult };

    default:
      return state;
  }
}*/
