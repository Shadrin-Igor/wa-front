export enum AsyncValidatorsType {
  FirmUniqEmail = 'firm-uniq-email',
  FirmUniqPhone = 'firm-uniq-phone',
  FirmUniqName = 'firm-uniq-name',
  FirmUniqLegalName = 'firm-uniq-legal-name',
}
