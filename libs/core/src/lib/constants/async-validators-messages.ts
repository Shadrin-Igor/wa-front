export enum AsyncValidatorsMessages {
  FirmUniqEmailInValidMessage = 'Указанный Email уже зарегистрирован в системе',
  FirmUniqEmailValidMessage = 'Указанный Email подходит',
  FirmUniqPhoneInValidMessage = 'Указанный телефон уже зарегистрирован в системе',
  FirmUniqPhoneValidMessage = 'Указанный телефон подходит',
  FirmUniqNameInValidMessage = 'Указанное название фирмы уже зарегистрирован в системе',
  FirmUniqNameValidMessage = 'Указанный название фирмы подходит',
  FirmUniqLegalNameInValidMessage = 'Указанное юридическое название уже зарегистрирован в системе',
  FirmUniqLegalNameValidMessage = 'Указанное юридическое название подходит',

  firmNameNotUniq = FirmUniqNameInValidMessage,
  firmEmailNotUniq = FirmUniqEmailInValidMessage,
  firmLegalNameNotUniq = FirmUniqLegalNameInValidMessage,
  firmPhoneNotUniq = FirmUniqPhoneInValidMessage,
}

